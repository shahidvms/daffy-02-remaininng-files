Lilly R. Adams
(404) 395-0951 lillyadams715@gmail.com
EDUCATION
University of Georgia
Grady College of Journalism and Mass Communication
Major: Advertising Major / Minor: Communications Studies Minor
Major GPA: 3.74/4.00; Overall GPA: 3.76/4.00
HOPE/Zell B. Miller Scholarship Recipient

May 2021
Athens, GA

EXPERIENCE
Talking Dog Agency
Sept 2020 March 2021
Account Executive | The Winning Edge Leadership Academy
Athens, GA
Serve as the communication liaison between the seven team members and Winning Edge Leadership to ensure client
needs are met and to serve as the face of the client for the team
Collaborate with the Winning Edge representative to generate multiple social media campaigns and increase
engagement by 223% in four months
Schedule bi-weekly calls and weekly meetings to set priorities for team and client
Help to create a plans book representing the accumulated social media graphics and data analytics to be presented to
the client
Dalton Agency
June 2020 Aug 2020
Account Management Intern
Atlanta, GA
Worked on a summer-long mock campaign for the reopening of Zoo Atlanta
Communicated with the internship supervisors, wrote creative briefs, and defined overarching goals, objectives and
strategies to carry out the tactics of the campaign
Performed competitive analysis against 5-10 competitors, researched market trends and participated in the creative
development process for 3 agency campaigns
CAMPUS & COMMUNITY INVOLVEMENT
UGA Ad Club
Jan 2019 Present
Member
Athens, GA
Enhance self-promotional and networking skills through opportunities to shadow/tour advertising agencies nationwide
Use industry-related content and professional connections to guide students through the transition from university to
careers in advertising
Develop campaign strategy skills to curate original ideas for brand re-builds, creative decks and persuasive imaging
Alpha Chi Omega Sorority
Jan 2019 Dec 2019
Social Chairman
Athens, GA
Conducted meetings with other UGA Greek Life organizations to plan around 10 social events for each semester and
discussed an execution plan for each event
Communicated to a chapter of 200+ women about events and analyzed feedback from fellow members to create growth
opportunities for the future
The Creative Circus
June 2019 Aug 2019
Summer at the Circus 8 Week Program
Atlanta, GA
Expanded my creative side of advertising by completing art compositions and creative projects
Collaborated with a team on more than 5 total advertising campaigns, diving deeper into copywriting and practicing the
selling of creative ideas through learned strategy
Advanced my skills in advertising by further understanding the digital marketing field through illustration, writing, and
graphic design courses
SKILLS
Skills: Interpersonal Communication, Account Management, Project Management, AP Style Writing, Adobe Suite Knowledge,
Microsoft Suite Knowledge
Certificates: Adobe Photoshop, Adobe InDesign, Google Analytics, Google Ads Search

��

-----END OF RESUME-----

Resume Title: Advertising/Public Relations - Account Management

Name: Lilly Adams
Email: lillyadams715@gmail.com
Phone: true
Location: Athens-GA-United States-30609

Work History

Job Title: Member 01/01/2019 - Present
Company Name: UGA Ad Club

Job Title: Account Executive 09/01/2020 - 03/31/2021
Company Name: Talking Dog Agency; The Winning Edge Leadership Academy

Job Title: Account Management Intern 06/01/2020 - 08/31/2020
Company Name: Dalton Agency

Job Title: Social Chairman 01/01/2019 - 12/31/2019
Company Name: Alpha Chi Omega Sorority

Job Title: Social Chairman 06/01/2019 - 08/31/2019
Company Name: The Creative Circus

Education

School: University of Georgia; Grady College of Journalism and Mass Communication, Major:
Degree: Bachelor's Degree, Graduation Date:

School: Athens, GA, Major:
Degree: None, Graduation Date:

Additional Skills And Qualifications

Most Recent Job Title: true
Most Recent Pay: USD per
Managed Others: true
Languages Spoken: English
Work Authorization: Not Specified
Security Clearance: No
Felony Conviction: No

Desired Pay: USD per
Desired Job Types: Full Time
Travel Preference:

Currently Employed: No
Jobs Last Three Years: 0
Last Job Tenure Months: 0
Highest Degree: None
Certifications:
Employment Type:
Military Experience:
Last Updated: 5/6/2021 12:00:00 AM

Desired Shift Preferences:

Interests:
Arts - Culture

Downloaded Resume from CareerBuilder
RD929F75TSWX7R03201


