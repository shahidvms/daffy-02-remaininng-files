EDWARD JAY BERGER
Phoenix, AZ | 602.801.8656 | Edwardberger04@gmail.com or Edward.J.Berger@outlook.com
https://www.linkedin.com/in/edward-berger-7158b781/

Sr. Cyber Security Engineer
Information Security | Systems Analysis | Governance Modeling
Active TS/SCI clearance
Intuitive and highly analytical customer centric Sr. Cyber Security Engineer with 12+ years of enterprise level expertise deploying and maintaining complex DoD networks comprised of various vendors. Results driven subject matter expert in organizational information assurance processes, enterprise services, and a first responder to incidents to assist in the collection of necessary information to conduct investigations. Specialized in prioritizing activities to achieve defined cyber security objectives while simultaneously working to keep projects within scope and budget; natural ability to translate business requirements into network security solutions.
Additional technical proficiencies include:
Active Directory | DNS | Group Policy Objects {GPO} | Hyper -V | Scripting & Automation | PowerShell |
Database Administration | HBSS | SCCM | ACAS | Cyber Security | Lifecycle Program Management | Quality Control | Business Development | Risk Management | Leadership

CERTIFICATIONS

CompTIA Security+ CE | CompTIA Network+ | CompTIA A+ | CompTIA Server+ | MCSA Windows Server
2012 | Microsoft Azure Fundamental VMWare Associate Data Center Virtualization | Server Virtualization
EDUCATION

MASTER OF SCIENCE IT Project Management, American Military University 2017-Present BACHELOR OF SCIENCE Logistics, Georgia Southern University ASSOCIATE OF APPLIED SCIENCE Information Technology, Community College of the Air Force
Professional Experience
UNITED STATES AIR FORCE, Albuquerque, NM
Sr Cyber Security Engineer (2017-Present)
Played a key role in maintaining highest levels of information security for Office of Secretary of Defense organization responsible for conducting classified rapid assessments of war-fighter capabilities. Leads five-member IT team across three locations, 15 network domains, and 800 systems, with responsibility for supporting daily technical operations, network administration, helpdesk services, and IT management.
Organized $1.5M remodel project for sensitive information facility, including more than 50 devices on unclassified and classified networks. Rebuilt permissions through File Server Management in Active Directory. Used PowerShell to remote and update computers.
Directed successful network migration project. Seamlessly transferred four classified networks, eight data systems, and over 300 PCs to new data node. Integrated SDC Windows 10.
Led $2M communication network upgrade project throughout entire lifecycle. Successfully patched 7 file management and print servers.

EDWARD JAY BERGER
Phoenix, AZ | 602.801.8656 | Edwardberger04@gmail.com or Edward.j.berger@outlook.com
https://www.linkedin.com/in/edward-berger-7158b781/

UNITED STATES AIR FORCE, Luke, AFB

Network Operations / System Administrator (2014-2017)
Completed system administration of $24M LAN while managing 69TB storage area. Maintained Active Directory tree for 18.3K users, performed WAVE server build. Managed COMSEC inventory, including validation of 86 crypto devices valued at $500K. Configured Retina Vulnerability Server to scan network for vulnerabilities. Experience with Solar Winds to see if certain network nodes are operational.
Designed and implemented SCCM solution to resolve missed vulnerability patches.
Implemented emergency backup server. Restored radar feed, saving training for 12 jet units.
Engineered circuits for $890K radar and radio project, linking coverage to eight sites (121K square mile airspace).
Created virtual server training environment utilizing VMware Hypervisor.
Assisted with configuration of a SCSI on a local VMWARE server.

UNITED STATES AIR FORCE, Osan AB, South Korea

Cyber Systems Supervisor (2013-2014)
Directed team responsible for organization s $2.2M cyber budget of network assets. Coordinated closely with manufacturers and vendors regarding specifications, requirements, and logistical issues. Trained team of three technicians.
Controlled Active Directory Domain Services for domains of over 1,000 users. Gained expertise in LDAP and SQL technologies.
UNITED STATES AIR FORCE, Ramstein AB, Germany

Enterprise Service Desk Technician (2009-2011)
Provided direct support to 850K users, processed 9.4K requests, and installed 1.3K vulnerability patches. Extended core network services by performing configuration and maintenance tasks on Microsoft servers. Developed training and tracked employee professional development.
Successfully resolved Traced Base Exchange server failure. Coordinated mail relay fix and restored classified capabilities in timely manner.
Oversaw $1.1M technology refresh with zero downtime.

-----END OF RESUME-----

Resume Title: Technology - Technical Support

Name: Edward Berger
Email: edward.j.berger@outlook.com
Phone: true
Location: Phoenix-AZ-United States-85001

Work History

Job Title: Sr Cyber Security Engineer 01/01/2017 - Present
Company Name: UNITED STATES AIR FORCE

Job Title: Network Operations / System Administrator 01/01/2014 - 01/01/2017
Company Name: UNITED STATES AIR FORCE

Job Title: Cyber Systems Supervisor 01/01/2013 - 01/01/2014
Company Name: UNITED STATES AIR FORCE

Job Title: Enterprise Service Desk Technician 01/01/2009 - 12/31/2011
Company Name: true

Education

School: American Military University, Major:
Degree: Master's Degree, Graduation Date:

Additional Skills And Qualifications

Most Recent Job Title: true
Most Recent Pay: USD per
Managed Others: true
Languages Spoken: English
Work Authorization: true
Security Clearance: No
Felony Conviction: No

Desired Pay: USD per
Desired Job Types: Full Time
Travel Preference:

Currently Employed: No
Jobs Last Three Years: 0
Last Job Tenure Months: 0
Highest Degree: Graduate Degree
Certifications:
Employment Type:
Military Experience:
Last Updated: 5/20/2021 12:00:00 AM

Desired Shift Preferences:

Interests:
Information Technology
Internet-E-Comerce

Downloaded Resume from CareerBuilder
RDF16N6BDDJ6N9W6QYJ


