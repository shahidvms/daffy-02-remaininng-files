VALERIE R. LYSTER

614.561.6720

vrlyster@gmail.com

 

CAREER SUMMARY

Highly motivated global sourcing and product development professional with additional merchandising background. I have managed the life cycle of garments from initial conception through production and sales for a variety of categories. Focusing on vendor relationships, talent development, processes, cost negotiations, maintaining timelines and creative problem solving. I have strong organizational, communicative and collaborative skills.

 

PROFESSIONAL EXPERIENCE

Ascena Retail Group (Lane Bryant)        2018-2020

Senior Sourcing Manager (Woven Tops, Sweaters and Activewear)

 

Developed talent through training and coaching direct reports
Cultivated and maintained strong relationships with overseas offices as well as market vendors and cross-functional teams. Helping to initiate best practices for more efficiencies
Explored new vendor partnerships both domestically and internationally and helped to onboard them with company policies and practices
Planned vendor allocation and penetration that was in line with company goals and objectives
Rebuilt and clarified outdated replenishment program for active bottoms to be more in line with our needs for both timing and risk of liability
Achieved up to 9% cost savings compared to past orders on key item programs by multi sourcing and creating competition among vendors
 

Ascena Retail Group (Lane Bryant)        2013-2018

Sourcing Manager (Intimates/Sleep/Swim, Woven Tops & Sweaters)

 

Responsible for negotiating costs, helping cost engineer garments and reach dept IMU goals while keeping integrity of the product designs
Developed talent through training and coaching direct reports
Managed product lifecycle from conception through production and delivery. As well as helping to react to sales through chase or cancellations
Created tools useful in costing negotiations such as a trim and fabric consolidation chart to help hit and communicate minimums as well as a fabric cost chart for staying within our affordability options by product category
Worked on pilot speed program as the “subject matter expert” to help deliver faster, more on trend fashion in reaction to sales.  Helped to later train my fellow associates on the program
 

Ascena Retail Group (Lane Bryant)        2012-2013

Associate Sourcing Manager (Intimates & Swim)

 

Managed product lifecycle from conception through production and delivery. As well as helping to react to sales through chase or cancellations
Responsible for negotiating costs, helping cost engineer garments and reach dept IMU goals while keeping integrity of the product designs
 

 

 

Abercrombie and Fitch          2009 – 2011

Senior Merchant (Sleepwear & Woven Tops)

 

Continuously analyzed business through past and current selling as well as in store product tests to ensure we were following current trends and maximizing profitability
Partnered with design and planning groups to build assortment of product that best represented our aesthetic and financial vision.  Advising needed styles and choice counts to design
Lead and coached several direct reports to understand merchandising and vendor relationships, creating strong merchants within our company
Responsible for negotiating costs, helping cost engineer garments, source materials and focus assortment.  Resulting in decreasing Gilly Hicks Sleep AUC by 43% over 2 years and increasing IMU by 14% in a dept struggling with MOQ issues
Traveled internationally to foster vendor and factory relationships and build processes
 

Abercrombie and Fitch          2007 – 2009

Merchant (Intimates/Sleep/Swim)

 

Developed and coached direct reports to build strong leaders within our company
Consistently sought out material options and ideas to help reduce cost while maintaining garment design integrity to achieve company aesthetic and cost goals
Integral member of a small team to launch Gilly Hicks brand through product and material research, competitive analysis, focus groups and new vendor onboarding for intimates category
Analyzed sales to adjust and build future orders to increase profitability for the company
 

Abercrombie and Fitch          2004 – 2007

Associate Merchant (Intimates/Sleep/Swim & Denim)

 

Achieved IMU goals through vendor cost negotiations, raw material sourcing and working with cross functional teams on engineering product
Pulled, analyzed and reported on weekly sales to help discuss current plans and future orders
Communicated issues and needs to overseas partners to maintain product quality and timelines
 

Abercrombie and Fitch          2003 – 2004

Assistant Merchant (Intimates/Sleep/Swim & Knit Tops)

Helped analyze weekly sales as well as color and size selling for future order adjustments
Wrote and maintained commits and purchase orders
Communicated daily with vendor partners on costing, timelines and production
Managed samples for meeting presentations, visual team and marketing pass offs
 

Abercrombie and Fitch (Stores)       2002-2003

Store Manager

 

Recruited, trained and managed staff of 50 associates and 4 managers to maintain high level of excellence in store presentation as well as meet sales goals.
Managed mock store, working closely with visual team to develop company standard floorsets
 

EDUCATION

Bachelor of Science, Retail Management/Merchandising, Purdue University 2001