Professional Summary

Skilled learning and development professional with extensive experience collaborating with business leaders to analyze organizational learning needs. Proven ability to analyze, design and develop dynamic and interactive learning solutions utilizing a wide variety of applications and approaches.

Skills

Exceptional written and verbal communication
Utilization of ADDIE, AGILE and Rapid ID development methods
Comprehensive needs analysis and assessment
Design document and storyboard creation
Dynamic delivery of live and virtual learning
Design, development and delivery of interactive eLearning
Extensive use of Learning Management Systems
Assessment creation
Collaborative project management
Broad curriculum design for large and small learning programs
Development of learning tools such as job aids, reference guides, posters, and checklists

Work History

Senior Learning Specialist 09/2015 to Current
W.S. Badcock, Mulberry, FL
Analyzed employee skills and needs resulting in timely and effective learning solutions for Retail, customer service, credit and finance compliance and leadership.
Trained and mentored interns and new training personnel resulting in their immediate, positive contribution to corporate training goals.
Used Articulate and Power Point to develop and deliver negotiation and collection training for accounts receivable employees, resulting in quarterly record setting collection numbers.
Created and oversaw new hire training programs for Credit Services, Accounts Receivable and Customer Service operations resulting in reduced turnover, a highly skilled workforce and a better customer experience.
Designed, developed and delivered corporate leadership program resulting in leaders who assist their employees in reaching their full potential both personally and professionally.
Created department training development templates and processes which contributed to a reduced development and delivery time and higher quality training materials.
Traveled to various geographic regions to deliver new store staff training resulting in store staff which was ready to provide our customers with a 'legendary experience'.
Designed and delivered organizational compliance learning program resulting in full compliance across the company as required by governmental agencies.
Utilized video, graphics and audio during learning development resulting in dynamic learning products which resulted in an increase in employee satisfaction, retention and skill development.
Senior Training Consultant 2014 to 09/2015
Aetna - Contract, Sarasota, FL
Served as a part of the overall implementation team for Rational Team Concert system roll out.
Managed the end to end training responsibilities for the rollout of the Rational Team Concert system.
Conducted job role and system analysis, resulting in the development of a comprehensive system training program for all user groups of the Rational Team Concert system.
Developed instructor led virtual system training including activities, resulting in effective skill development for employees who were located in various geographic areas.
Planned and executed the communication and launch of Agile system training resulting in a 100% required user attendance.
Delivered instructor led role-based virtual learning, for Rational Team Concert training which provided skills and knowledge necessary for Agile team members to make an immediate and valuable contribution during sprints.
Training Specialist 07/2012 to 07/2014
Verizon - Contract, Sarasota
Developed New Hire and System Training for Alternate Sales Channels allowing new employees to make an immediate contribution to company goals.
Delivered product and promotion training virtually, ensuring the establishment of an informed sales force.
Hosted weekly communication meetings for Sales channels resulting in a skilled and knowledgeable sales staff.
Design and developed Job Aids for use by alternate sales channels.
Lead Training Specialist 2011 to 2012
JP Morgan Chase - Contract, Chicago, IL
Conducted training needs analyses for corporate migration product, resulting in clearly defined role based training needs.
Designed corporate process training, using high- and low-level design document strategies resulting in a well defined training plan.
Developed both instructor-led and computer-based training to which assisted employees with the skill to effortlessly assist customers in migrating their business.
Mentored team of 6 instructional designers to confirm programs were being developed appropriately, accurately, and in conformity with corporate goals, and created an ADDIE-based training development process for the entire team to use.
Collaborated with technical documentation specialists to develop and curate the documentation process.
Training Specialist 01/2004 to 2011
Verizon -Contract, Sarasota, FL
Partnered with cross-functional teams to analyze performance issues, resulting in the development of learning programs which improved skills across user groups.
Developed systems and tools learning solutions for Customer Service, Sales, and Retail employees, using a variety of strategies and techniques including paper based, blended, e-learning, and podcast training. This effort allowed training to be delivered globally and in a manner which suited all learning styles.
Reviewed, and researched technical content through collaboration with Subject Matter Experts and the use of technical documentation. This resulted in the establishment of trust between corporate employees and the training team.

Education

Master of Science: : Adult Education For Online Learning
Capella University - Minneapolis, MN
Associate of Arts: : History Teacher Education, 05/1994
Eastern Illinois University - Charleston, IL

.

.
.
.
.

image1.png

[PNG Image]

-----END OF RESUME-----

Resume Title: Senior Learning Specialist

Name: Nicholas Falcetta
Email: nickfalcetta@gmail.com
Phone: (863) 279-6276
Location: Tampa-FL-United States-33603

Work History

Job Title: Senior Learning Specialist 09/01/2015 - Present
Company Name: W.S. Badcock

Job Title: Senior Training Consultant 01/01/2014 - 09/01/2015
Company Name: Aetna

Job Title: Training Specialist 07/01/2012 - 07/31/2014
Company Name: Verizon

Job Title: Lead Training Specialist 01/01/2011 - 12/31/2012
Company Name: JP Morgan Chase

Job Title: Training Specialist 01/01/2004 - 01/01/2011
Company Name: Verizon

Education

School: Capella University - Minneapolis, MN, Major:
Degree: Master's Degree, Graduation Date:

School: Eastern Illinois University - Charleston, IL, Major:
Degree: None, Graduation Date:

Additional Skills And Qualifications

Most Recent Job Title: true
Most Recent Pay: USD per
Managed Others: true
Languages Spoken:
Work Authorization: Not Specified
Security Clearance: No
Felony Conviction: No

Desired Pay: USD per
Desired Job Types: ["Full Time","Part Time","Intern","Seasonal","Temporary","Contractor"]
Travel Preference:

Currently Employed: No
Jobs Last Three Years: 0
Last Job Tenure Months: 0
Highest Degree: None
Certifications:
Employment Type:
Military Experience:
Last Updated: 3/1/2021 8:09:44 PM

Desired Shift Preferences:

Interests:
General Business
Supply Chain
Business Development
Consultant
Strategy - Planning
Telecommunications
BPO-KPO

Downloaded Resume from CareerBuilder
R2S0ZJ71NKSQ9J3TTVB


