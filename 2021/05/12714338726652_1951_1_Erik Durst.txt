ERIK DURST
Phoenix, AZ | 480-785-6091 | erik.durst@outlook.com | https://www.linkedin.com/in/erikdurst

IT SENIOR MANAGER
Detailed IT leader with several years of experience managing user systems and solutions for multiple Fortune 500
companies. Excellent project management experience implementing major IT system environment upgrades with
large CAPEX and user audiences. IT desktop and security process creation, enforcement, audit and remediation
across a corporate enterprise. Also direct senior-executive and leadership system support including IT policy
management experience.
CRITICAL SKILLS
Team Leadership | Support End-User Desktop and Mobility | System Infrastructure |
Corporate Enterprise | Project Management | Incident, Problem and Outage Escalation |
IT Security | International Solution Collaboration | Microsoft Suite | Exchange |
SharePoint | Cisco Network | VMware | Server | SOC 2 Audit | ITIL
PROFESSIONAL EXPERIENCE
Deutsche Telekom AG / T-Systems North America
Team and Projects Manager |
May 2018 - January 2021
Covered internal IT services and solutions for T-Systems North America as well as affiliated DTAG business
groups (Deutsche Telekom North America and Deutsche Telekom International Inc).
Managed advanced engineering team supporting the internal workstation, mobile, application and IT
infrastructure for the DTAG/T-Systems North America group.
Monitored end-user service stability (including VPN connectivity and Exchange access) as well as outage
management and IT security and policy compliance.
Worked directly with senior leadership on topics of internal CAPEX budget and IT vendor service
agreements in excess of $200K.
Project management included physical and Microsoft Azure virtual server builds as part of 500 user
workstation deployment to Windows 10 and Office365. Migration effort included system delivery, enduser training and data move while accommodating corporate logistical limitations due to COVID-19
global health crisis.
Conducted user account and data migration to new Microsoft Exchange Online and SharePoint Online
infrastructure. For SOC 2 certification, active leader in the yearly external vendor audit of internal
processes for DTAG/T-Systems North America.
Reorganization projects involved coordinating IT service shutdown for selected DTAG/T-Systems North
America offices as well as legacy DNS network and server decommissioning work.
Team Manager |
January 2017 - May 2018
Expanded IT management role covering internal end-user support for the DTAG/T-Systems North America
business groups.
Conducted project management to upgrade 500 workstations to Microsoft Office 2016 suite as well as
separate mobile iPhone upgrade effort.
Coordinated, build, and deployment of new building-facility Milestone Windows 2016 physical servers
with network reconfiguration at all T-Systems North America offices.
Team Lead |
July 2011 - January 2017
Promoted to leader supporting internal desktop environment for DTAG/T-Systems North America. Managed over
1000 internal workstations and mobile devices for T-Systems North America, DT North America and DTI Inc.
affiliated groups.
Implemented large $500K+ CAPEX project for replacement of workstations and mobile hardware across
the North America corporate group, including new print-server and antivirus management solutions.

�Erik Durst

Page 2

Oversaw the advanced system engineering group supporting desktop services for T-Systems North
America. Cross-trained the first level support desk hosted in Mexico.
Escalation contact for internal IT support services involving senior leadership and executives.
Administered local corporate IT security and desktop policies.

Senior Engineer |
January 2010 - July 2011
Project to transition IT services from Deutsche Post-DHL US to Deutsche Telekom/T-Systems North America.
Project included user workstation and mobile support as well as field engineering and internal application
solutions incorporating over 10,000 external shipping customer workstations.
Services for workstation and mobile environment transition included alignment with Deutsche
Telekom/T-Systems North America support resources, policies and procedures to ensure minimal
Deutsche Post-DHL US customer impact.
Deutsche Post / DHL Global Business Services
Team Lead |
March 2007 - January 2010
Leadership of the Deutsche Post-DHL US internal desktop support group as well as executive and management
escalation point of contact for internal workstation policy and procedures.
Served as United States point of contact for global Deutsche Post-DHL support processes and advanced
IT system-problem troubleshooting.
Continued executive user support as well as project management and documenting corporate IT policies.
System Administrator |
May 2005 - March 2007
Participated in advanced user support, and produced internal support policies and procedures for Deutsche PostDHL US customer workstation environment in excess of 10,000 systems.
Project management of new hardware and software evaluations for corporate use. Administration of the
Altiris solution used to manage workstations across the DHL US enterprise.
Administered the Symantec antivirus solution in deployment at the DHL IT center. Created and
maintained intranet site for end-user support.

EDUCATION
University of Washington, Seattle | Bachelor of Science Physics

CERTIFICATIONS
Microsoft Certified IT Professional (MCITP)
Microsoft Certified System Administrator (MCSA)
Microsoft Certified Professional (MCP)
CompTIA Network+
Skill Management and ITIL Foundations

Microsoft Certified Technology Specialist (MCTS)
Microsoft Certified Desktop Support (MCDST)
CompTIA A+
CompTIA Security+

�

-----END OF RESUME-----

Resume Title: Technology - Technical Support

Name: Erik Durst
Email: erik.durst@outlook.com
Phone: true
Location: Phoenix-AZ-United States-85001

Work History

Job Title: Team and Projects Manager 05/01/2018 - 01/31/2021
Company Name: Deutsche Telekom AG / T-Systems North America

Job Title: Team Manager 01/01/2017 - 05/01/2018
Company Name: Deutsche Telekom AG / T-Systems North America

Job Title: Team Lead 07/01/2011 - 01/01/2017
Company Name: Deutsche Telekom AG / T-Systems North America

Job Title: Senior Engineer 01/01/2010 - 07/01/2011
Company Name: Deutsche Telekom AG / T-Systems North America

Job Title: Team Lead 03/01/2007 - 01/01/2010
Company Name: Deutsche Post-DHL US; Deutsche Post / DHL

Job Title: System Administrator 05/01/2005 - 03/01/2007
Company Name: true

Education

School: University of Washington, Major:
Degree: Bachelor's Degree, Graduation Date:

Additional Skills And Qualifications

Most Recent Job Title: true
Most Recent Pay: USD per
Managed Others: true
Languages Spoken: English
Work Authorization: true
Security Clearance: No
Felony Conviction: No

Desired Pay: USD per
Desired Job Types: Full Time
Travel Preference:

Currently Employed: No
Jobs Last Three Years: 0
Last Job Tenure Months: 0
Highest Degree: 4 Year Degree
Certifications:
Employment Type:
Military Experience:
Last Updated: 4/15/2021 12:00:00 AM

Desired Shift Preferences:

Interests:
Information Technology
Internet-E-Comerce

Downloaded Resume from CareerBuilder
RD71V66KD8X6079J93V


