Tugba Bekar

Quality Assurance Test Engineer

Mobile: (913) 735-9556 Email: tugbabekar46@gmail.com LinkedIn: www.linkedin.com/in/tugbabekar

Career and Skillset

Hands-on skills on UI and API applications while performing manual and automation test cases. Database-server testing through SQL, API testing on Postman and automation testing skills with back-end, front-end testing while utilizing Java language. Practiced in creating frameworks from scratch and automating test cases with Selenium WebDriver, Cucumber Behavior-Driven / Data-Driven, JDBC, API through the SDLC process. Utilizing REST Assured, TestNG, JUnit, Gherkin, JDBC, Apache POI, GSON, WebDriver Manager, Maven dependencies in the frameworks by following POM (Page Object Model) design. Producing informative reports through CI/CD process by using Jenkins and GitHub. Familiar with Agile-Scrum methodologies and tools such as Jira.

Professional Work History

Background

Cybertek Schools, McTyson, VA

SDET/QA, November 2019 - August 2020

Dedicated and skilled hands-on Quality Assurer in IT industry. A boot Camp graduate student in a mock agile/scrum working environment. Excellent skills while working in a cross-functional team or individual work environment. Very good at analyzing skills and quality-oriented mindset.

Project: Vy-Track Application

Practices on fleet management application.

Using the Project Object Model (POM) structure on frameworks to clear framework from unclarity and reuse the created objects, frameworks by using Java language and applying OOP Java concepts
Experience in various testing tools, automation, acceptance testing. Experienced in Front-End testing structures. Using TestNG libraries to run the tests and completing XML file with configuration property files to run specific tests
Used Selenium Grid for running tests across different browsers. Using Eclipse / IntelliJ as IDE and run tests in different browsers such as Chrome, Firefox, Safari. Used Jira management tool as the bug tracking tool and GITHUB as the version control system
Practiced skills and knowledge-based in Software Development Life Cycle (SDLC), Software Testing Life Cycle (STLC)

Experience in Requirement Specifications (SRS) and Test Scenarios in the Agile project while attending Sprint Ceremonies such as Sprint Planning meeting, Scrum, Backlog Grooming, Sprint Review, and Retrospective

Project: Book-it Application

Practices on book-in management.

As a Cucumber and API Automation Engineer, professionally worked on Manual, Automation, DevOps Testing of Web Client Server, API, Web services. Experience in various testing tools automation acceptance such as Cucumber, BDD, DDT, Hybrid, Excel Data-Driven, JDBC, SQL, and API testing in the Used Gherkin Language in Cucumber framework by developing Scenarios and Scenario Outlines in Feature files. Run classes in CukesRunner with RunWith/CucumberOptions by specifying the scenario tags while using JUnit.

Experience in creating, modifying and maintaining new or existing automation frameworks by using Java language and applying OOP Java concepts
Actively used Excel Data-Driven Test and JDBC In some cases to produce healthier test cases. Used Apache POI excel libraries to read, write and get data.
Experienced API concepts in test cases with the help of the manual test tool Postman.

Used Restful Assured libraries in API and post/get/update/delete data via special commands such as POST, GET, DELETE, PUT.

Practiced skills and knowledge-based in Software Development Life Cycle (SDLC), Software Testing Life Cycle (STLC)

Experience in Requirement Specifications (SRS) and Test Scenarios in the Agile project while attending Sprint Ceremonies such as Sprint Planning meeting, Scrum, Backlog Grooming, Sprint Review, and Retrospective.

Background

Volunteer Experience: Raindrop Foundation, San Antonio, TX

Event Organizer, Ethics Education Teacher, August 2014 - July 2018

The Raindrop Foundation is dispersed in America and serves the community in a very wide spectrum. Voluntarily worked for the community in many areas which is provided by the foundation.
Event Organizer, Art and Music Performer Taught Kindergartner, Middle Schoolers, High Schoolers and University Level Ethics and Education Classes. Prepared class materials to be taught in Turkish Language, created PPT, Video Informative's etc.
Organized events and activities for the Kids Club students and parents. Proudly participated the Raindrop Ensemble Musician Club as a drum player Performed as well as taught Water Marbling and Polymer Clay arts in public Organized Coffee Night for the community

Background

Intern Teacher, Konya, Turkey

Ethics & Religious Studies Teacher, December 2013 - June 2014

Ethics & Religious Studies Teachers guides students while they are growing and learning about moral and character topics in their real life. As an ERS Teacher, I taught ethics and character concepts middle-High schoolers. Log student behaviors and track them with the mentor teacher.

Toolbox

Framework:

Cucumber, Selenium WebDriver, JDBC, API-POI, Maven, POM Bug TrackingTools:
JIRA

Dependencies:

JUnit, Cucumber-Java, Selenium WebDriver Manager, TestNG, JDBC, GSON, REST Assured Languages:
Java, HTML

Version Control:

GITHUB

Continuous Integration-Deployment: Jenkins Cross Browser Platforms:

Selenium GRID

IDE:

Eclipse, IntelliJ, Postman Environment :

Slack, Gmail

Database :

Oracle SQL

Education Cybertek School SDET / QA Bootcamp McLean, VA, USA

Bachelor of Arts:

Necmettin Erbakan University - Ethics & Religious Studies Teacher, Konya, Turkey

-----END OF RESUME-----

Resume Title: Education - Teacher - General Education - K-12

Name: Tugba Bekar
Email: tugbabekar46@gmail.com
Phone: true
Location: San Antonio-TX-United States-78289

Work History

Job Title: SDET/QA 11/01/2019 - 08/31/2020
Company Name: Cybertek Schools

Job Title: Event Organizer, Ethics Education Teacher 08/01/2014 - 07/31/2018
Company Name: Raindrop Foundation

Job Title: Intern Teacher 12/01/2013 - 06/30/2014
Company Name: true

Education

School: Cybertek School SDET / QA Bootcamp McLean, VA, USA, Major:
Degree: Bachelor's Degree, Graduation Date:

School: Necmettin Erbakan University; Konya, Turkey, Major:
Degree: Bachelor's Degree, Graduation Date:

Additional Skills And Qualifications

Most Recent Job Title: true
Most Recent Pay: USD per
Managed Others: true
Languages Spoken: English
Work Authorization: Not Specified
Security Clearance: No
Felony Conviction: No

Desired Pay: USD per
Desired Job Types: Full Time
Travel Preference:

Currently Employed: No
Jobs Last Three Years: 0
Last Job Tenure Months: 0
Highest Degree: 4 Year Degree
Certifications:
Employment Type:
Military Experience:
Last Updated: 3/23/2021 12:00:00 AM

Desired Shift Preferences:

Interests:
Information Technology
Internet-E-Comerce

Downloaded Resume from CareerBuilder
RCL5TF6CGBRR61MM1LX


