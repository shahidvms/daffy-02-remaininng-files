Gina Fountain - SchneiderPrint 
Customer Service Agent, Shelter Insurance
Crestwood, KY 40014
US
Home: 502-594-7709
Mobile: -
gigimarie15@yahoo.com
Send Message

Add to folder
Print
Forward resume
Add note
Rate selected
Update Status
Block resume
Report Resume
Resume
Additional Info
Skills
Resume Updated
5/22/2021
Viewing Resume: nuyr7hrng2rci3db | Posted 5/22/2021
Download
 Word
Resume ID nuyr7hrng2rci3db
Gigimarie-nuyr7hrng2rci3db
 








OBJECTIVE:	
Dedicated purchasing professional specializing in logistics coordination, technical vendor & customer relations and inventory management. Organized and proactive with remarkable communication and planning abilities. Exceptional leader, skilled at reducing costs and increasing department efficiency.
I have a wealth of personal knowledge and interest of world events, science, metallurgical engineering and military locations/workings. One should never stop learning.

EXPERIENCE:	4/2019 - 10/2019	Shelter Insurance	
Customer Service Agent
Performs duties associated with correcting claims transaction errors. Assists with answering claims-related inquires to ensure resolution of challenges. Directs and governs vendor system for Claims systems. This includes vendor entry, verification of vendor data, and communication with claims personnel regarding vendor issues.
11/2014 - 2/2017	Omagi Salon and Spa	
Aveda Procurement Manager
Omagi Salon and Spa is a multi-time, Top 200 award winning, million-dollar, Aveda salon in the US. Procurement Manager is responsible to plan, direct and coordinate the process of ordering/receiving materials and products. Maintain current databases. Run reports and analyze data. Communicate with team regarding suggestions, ideas, challenges. Review weekly retail counts, check with team to determine/discuss discrepancies. Communicate with vendors regarding backorders/price change/returns/sourcing availability. Plan ahead according to season and occasion.
9/2013 - 8/2014	Norton Hospital	
Customer Service Agent
Warmly greeted hospital guests, gave directions inside/outside the hospital, A source of information & comfort for guests and employees finding answers, A human face/embodiment of all things "Norton"

EDUCATION:	0/1990 - 0/1995	Bellarmine College	
Bachelor's Degree
Bellarmine College
1990 - 1995
B.A. Communications

CERTIFICATION:	license






Additional Info	 
Target Company:	Company Size:	
Target Locations:	Selected Locations:	US
Relocate:	No
Willingness to travel:	No Travel Required