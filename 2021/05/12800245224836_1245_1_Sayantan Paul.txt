Sayantan Paul
Senior Mainframe Developer / Technical Lead

PERSONAL DETAILS PERSONAL SUMMARY
Sayantan Paul Mainframe Developer with extensive knowledge in AGILE and 12 years of
diverse IT experience in the US Healthcare and Insurance (Business and
215 Tudor Lane Property) industries. SAFe certified Agilist with a comprehensive
Apartment G understanding of Business Stakeholders expectation, working with developers,
Manchester. Architects, Program Managers, End Users and Cross Functional and
Connecticut - 06042 Middleware Teams to deliver Enterprise software.
Cell: 860-328-0311
Experience in Mainframes, CoolGen (CAGEN), COBOL, SQL, CICS and
Email:paulsayantan1985@gmail.com DB2.

Experience in the areas of IT Project Management, Program Management,
Estimation, SoftwareArchitecture, Design and Development,
QualityManagement, RiskManagement,Mentoring, working in Onshore-
AREAS OF EXPERIENCE Offshore model, Building and Guiding Team, PI Planning and Tracking.

Mainframes, Z/OS, Seeking a new and challenging Mainframe Developer position, which will
COBOL, CAGEN (CoolGen), SQL, make best use of my existing technology, leadership and team building skills,
CICS, DB2, and my SAFe experience to help in my further development and growth of my
JCL, VSAM professional self.

PROFESSIONAL SUMMARY
PROFESSIONAL
Mainframe Developer: 12+ years of experience as a Mainframe Developer for
SAFe certified Agilist solutioning business problems and requirements into IT Solutions. Worked and
led team on decision-making, building enterprise software to meet
Certified in Python from University of stakeholder/customer/business demands.
Michigan

SDLC Technology Lead: 5 years of experience in IT solutioning for business
problems and opportunities for Business Insurance industry in the QRI and
Agile Claims areas and in the Healthcare industry in the network and provider areas
using Scrum, Kanban and SAFe methodologies and COBOL and CAGEN in
Project & Program Management
Z/OS Mainframes. Very strong experience and knowledge in Healthcare
Leadership Skills (Claims and Network and Provider domain) Industry and Insurance
(Property - Home Owners insurance, Business - Business Auto, Natioanl
Experience in Onshore Offshore model and International Middle Markets)

Onshore Deelopment lead: 4 years of experience as a development lead from
Onshore for offshore team in Business Insurance projects and also Healthcare
projects.

SAFe AGILE Tech Lead: 3 years of experience in leading offshore team in
Healthcare project working with multiple Product Owners for multiple
interdependent applications.

Vast experience of working in all phases of SDLC and expertise in Project
Management. Also, having work experience in projects using Agile
methodology.
WORK EXPERIENCE

PROJECT NAME: ENTERPRISE NETWORK AND PROVIDER DATA
(JUNE 2018 PRESENT)
PERSONAL SKILLS This application is a repository of enterprise wide Network and Provider Data
and their related information. Multiple cross functional downstream
Solution Driven application are dependent on this data and communicate via numerous
mainframe APIs and jobs.
Creativity & Innovation
Role: ONSHORE TECHNICAL LEAD
Decision Making

Strong Analytical Skills . Responsible for leading and assisting cross-functional software development
teams responsible for project delivery and bringing features to market.
Team Building Facilitate the Project Team technically through the agile development process
of release planning, sprint planning, daily standups meetings and
retrospectives. Responsible for planning and developing technical solutions
for business requirements. Also responsible for removing any impediments to
TECHNOLOGY SKILLS help the delivery teams meet their sprint commitments.

Operating Systems: Windows, Communicate with Business stakeholders to understand the business
z/OS problems and their priorities.
Translate busness needs to Technology solutions to be implemented to
provide tangible product enhancements for business and add value
Plan for whole PI to produce Business value in solutions for stakeholders
Languages: COBOL, CAGEN based on their priority and team capacity
Help drive best practice use of agile methodology as a core to the
(CoolGen), Python, C, C++,
development life cycle.

Sprint planning, daily stand-up meetings, grooming of stories.
Requirement Analysis, creating stories and defining user acceptance criteria.
Reporting to Leaderships, stakeholder with the latest status.
Ensure the development team is practicing core agile principles of
Project Management Tools:
collaboration, prioritization, team
accountability, and visibility.

Rally, IBM RTC. Making appropriate commitments through story selection and task definition.
Participate proactively in developing and maintaining team standards and
best practices.

Proactively remove any impediments preventing forward movement on
projects.
Technology Highlights: Z/OS, Mainframes, COBOL, CAGEN (CoolGen), SQL,
DB2,CICS, JCL, VSAM

PROJECT NAME: BUSINESS AUTO INSURANCE (MARCH 2017 JUNE 2018)

Enhance and maintain the QRI (Quote, Rate and Issue) platform for Business
Auto insurance for a leading Insurance provider in the United States.

Role: ONSHORE TECHNICAL LEAD

. Responsible for having regular connects with business for understanding their
pain points and translating them to technical solutions
. Leading team to develop and build the solutions to achieve the business
value
. Preparation and maintaining of documentation to maintain appropriate
project artefacts
. Technology Highlights: Z/OS, Mainframes, COBOL, CAGEN (CoolGen), SQL,
DB2, JCL.
WORK EXPERIENCE

CERTIFICATIONS PROJECT NAME: NATIONAL PROPERTY WORKSTATION (MARCH 2014
MARCH 2017)
SAFe? 4 Certified Agilist
INS21 Certified for Insurance Create and enhance the Rate, Quote and Issue application for National
Property Workstation for National Middle Market Business Insurance
Certified in Programming in
Role: Offshore Technical Lead
Python in certification
recognized by University of Responsible for development of requirement user stories which coming from
Michigan Product Owner
Work closely with Business Analyst in Scrum team to understand the
acceptance Criteria and develop the technical functionalities for the stories
Manage sprint (Scrum) related issues, and resolving other issues to meet the
targeted sprint velocity.
User story sizing to assess work effort on a sprint-by-sprint basis.
Technology Highlights: CAGEN (CoolGen), COBOL, SQL, DB2

PROJECT NAME: BUSINESS AUTO INSURANCE (JANUARY 2013 - MARCH
2014)

Enhance and maintain the QRI (Quote, Rate and Issue) platform for Business
Auto insurance for a leading Insurance provider in the United States.

Role: Developer

Develop business requirements into technical solutions
Follow Waterfall methodology
Perform Unit testing and documentation for functionalities developed
Identification and development of technical components and reusable
artifacts to reduce the application coding effort.
Technology Highlights: COBOL, SQL, DB2, JCL

PROJECT NAME: PROPERTY INSURANCE FOR HOMEOWNERS (NOVEMBER
2011 - DECEMBER 2012)

Enhance and maintain the QRI (Quote, Rate and Issue) platform for Home
Owners insurance for a leading Insurance provider in the United States.

Role: Developer

Develop business requirements into technical solutions
Develop the whole Rating Engine for the HomeOwners insurance as per
business directions
Follow Waterfall methodology for software development
Perform Unit testing and documentation for functionalities developed
Identification and development of technical components and reusable
artifacts to reduce the application coding effort.
Technology Highlights: COBOL, SQL, DB2, JCL, CICS
WORK EXPERIENCE

AWARDS & ACCOLADATES PROJECT NAME: CLAIMS SUBMITTED VIEW (AUGUST 2009 - NOVEMBER
2011)
Infosys Most Valuable Player

Award, November2011 Maintain Repository of submitted view of claims for a leading Healthcare
company in the United States
Cognizant Shining STAR
Role: Developer
Award, September2014
Worked closely with Onshore lead and Offshore lead to develop software
requirements
Complete unit testing and Integration testing and prepare corresponding
LANGUAGES documentation
Technology Highlights: COBOL, SQL, DB2, JCL, VSAM, CICS
English (Speak, Read & Write)

Hindi (Speak, Read & Write)

Bengali (Speak, Read & Write)

ACADEMIC QUALIFICATIONS

INTERESTS
Degree College/University Board / Year
Photography Unversity
B.TECH Meghnad Saha Institute West Bengal 2008
Film Making of Technology University of
Technology
Creative Writing 12th Science Council for 2004
Calcutta Boys School the Indian
Poetry School
Certificate
Cooking Examination
10th Science Council for 2002
Calcutta Boys School the Indian
School
Certificate
Examination

-----END OF RESUME-----

Resume Title: Project Management Tools

Name: Sayantan Paul
Email: paulsayantan1985@gmail.com
Phone: (860) 328-0311
Location: Wallingford-CT-United States-06492

Work History

Job Title: true 06/01/2018 - Present
Company Name: true

Job Title: ONSHORE TECHNICAL LEAD 01/01/1900 - 05/11/2021
Company Name: Creativity & Innovation

Job Title: ONSHORE TECHNICAL LEAD 01/01/1900 - 05/11/2021
Company Name: true

Job Title: Valuable Player 01/01/1900 - 05/11/2021
Company Name: Infosys Most

Job Title: true 11/01/2011 - 05/11/2021
Company Name: Award

Job Title: Developer 09/01/2014 - 05/11/2021
Company Name: Cognizant Shining STAR

Job Title: Product Owner 03/01/2014 - 03/31/2017
Company Name: NATIONAL PROPERTY WORKSTATION

Job Title: Developer 01/01/2013 - 03/01/2014
Company Name: true

Job Title: Developer 11/01/2011 - 12/31/2012
Company Name: true

Job Title: true 08/01/2009 - 11/01/2011
Company Name: AWARDS & ACCOLADATES

Education

School: Unversity, Major:
Degree: None, Graduation Date:

School: Meghnad Saha Institute West Bengal; University of Technology, Major:
Degree: Bachelor's Degree, Graduation Date:

School: Calcutta Boys School the Indian Poetry School, Major:
Degree: High School, Graduation Date:

Additional Skills And Qualifications

Most Recent Job Title: true
Most Recent Pay: USD per
Managed Others: true
Languages Spoken:
Work Authorization: Not Specified
Security Clearance: No
Felony Conviction: No

Desired Pay: USD per
Desired Job Types: ["Full Time","Part Time","Intern","Seasonal","Temporary","Contractor"]
Travel Preference:

Currently Employed: No
Jobs Last Three Years: 0
Last Job Tenure Months: 0
Highest Degree: High School Degree
Certifications:
Employment Type:
Military Experience:
Last Updated: 5/10/2021 5:37:13 PM

Desired Shift Preferences:

Interests:
Information Technology
Internet-E-Comerce

Downloaded Resume from CareerBuilder
R2R1S665FYMKNZ8CQTW


