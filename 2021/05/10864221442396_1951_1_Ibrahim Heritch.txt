Ibrahim H
eritch

10323 Whiteside dr. Apt: c Charlotte NC, USA
Phone: (704) 907-9163 - Github: IbrahimHeritch - LinkedIn: Ibrahim Heritch -Email: Ibrahimfh555@Gmail.com

Skills
Algorithm Design and Analysis

Reached 43rd Annual World Finals of the International Collegiate Programming Contest Finals

Taken Parallel & Distributed Computing course at UNCC to learn about parallel algorithm design
Software Engineering and Development

Experienced with Agile Development Process , Scrum process, and Test Driven Development .

Developed Software in a variety of technologies including Java , C#(.NET) , Node js, and Python
Relational Database Design

Designed and implemented a fully functional airport database in Oracle DBMS

Proficient in using both SQL and N
oSQL databases
Mobile App Development

Extensive experience both developing and teaching development for Android

Build cross-platform mobile applications using the Flutter framework
Web Development

Created web applications using Spring Boot MVC , Flask Python , and Node js

Significant knowledge of Networking and Internet Protocols .
. Operating Systems

Highly Knowledgeable about Windows and Linux operating systems.

Some experience with macOS

Work Experience:
Teaching Assistant, the University of North Carolina at Charlotte Charlotte, NC, USA (Sept 2020 - present)
- Teaching Assistant at Software Engineering class (ITSC 3155)
- Help Student Learn about agile development, git, and software engineering in general
Android Summer Camp Lab Assistant, Lebanese American University Beirut, Lebanon (Summer 2018)
- Assisted in teaching android development to highschool students
- Configuring and debugging android studio.
Secretary of the Computer Science Club, Lebanese American University Beirut, Lebanon (2018 - 2019)
- Lead a club of over 90 students from all backgrounds that have an interest in computer science
- Organize a variety of events to the benefit of both the community and club members
- Personally taught several workshops relating to software development

Education:
The University of North Carolina at Charlotte
achelor of Science in Computer Science with a concentration in Software, Systems, and Networks
B
Minor in Actuarial Sciences
Expected Graduation Date: May 2021
GPA: 3.5

Notable Projects:
roductivity App
P

Produce4U

Ancestor Records

A cross-platform mobile app
A website that connects people
A website for recording your family
that tracks your productivity.
With fresh produce from local food producers. history in the form of blogs that you
App has a flutter frontend and
So, they can buy fresh local food.
can share with your friends and
a firebase backend. It features
The website is made with react, Node js,
family. The website made using
synchronization across multiple
and MySQL.
Spring MVP with MySQL for the
devices and offline data persistence.
Database.

�

-----END OF RESUME-----

Resume Title: Technology - Developer, Programmer, Engineer

Name: Ibrahim Heritch
Email: ibrahimfh555@gmail.com
Phone: true
Location: Charlotte-NC-United States-28246

Work History

Job Title: Teaching Assistant 09/01/2020 - Present
Company Name: University of North Carolina

Job Title: Android Summer Camp Lab Assistant 07/01/2018 - 03/21/2021
Company Name: Lebanese American University

Job Title: Secretary of the Computer Science Club 01/01/2018 - 12/31/2019
Company Name: Lebanese American University

Education

School: University of North Carolina at Charlotte, Major:
Degree: None, Graduation Date:

Additional Skills And Qualifications

Most Recent Job Title: true
Most Recent Pay: USD per
Managed Others: true
Languages Spoken: English
Work Authorization: Not Specified
Security Clearance: No
Felony Conviction: No

Desired Pay: USD per
Desired Job Types: Full Time
Travel Preference:

Currently Employed: No
Jobs Last Three Years: 0
Last Job Tenure Months: 0
Highest Degree: None
Certifications:
Employment Type:
Military Experience:
Last Updated: 3/21/2021 12:00:00 AM

Desired Shift Preferences:

Interests:
Education
Training
Teaching

Downloaded Resume from CareerBuilder
R2R30H6NLD5ZYKH1KH9


