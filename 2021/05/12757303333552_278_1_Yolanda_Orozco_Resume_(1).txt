
Yolanda Orozco

Clewiston, FL 33440
954-534-3733
yoyorozco68@hotmail.com

OBJECTIVE

To obtain a position in which I could utilize my skills and knowledge and be a valuable asset to the company.

Skills

Manufacturing
Packaging
Quality Assurance
Equipment Assembly

Machine Flow Regulation
Measurements
Documenting/Recording Data
Minor/Mayor Cleaning

Experience

March 2001 to December 2012
Schering Plough/Merck Pharmaceuticals  Miami Lakes , 33014-3103
Pharmaceutical Manufacturing Operator    

Tested product samples for specific gravity, chemical characteristics, pH levels, concentrations and viscosities
Performed manufacturing duties associated with coating, PB1, PB2, PB3, Cushion Grip, Lamination, Print Score, and Slitting
Adhered to all safety procedures and protocols when using equipment and moving hazardous chemicals to prevent mishaps and accidents
Observed gauges, recording instruments and flowmeters to maintain specified conditions
Accurately entered data into batch ticketing system
Identified issues promptly and notified the line leader immediately regarding potential project delays
Added neutralizing agents to products and pumped products through filters to remove impurities
Kept team on track by assigning and supervising their activities and giving constructive feedback
Demonstrated leadership by making improvements to work processes and helping to train others
Worked closely with team members to deliver project requirements, develop solutions and meet deadlines
Contributed to development, planning and completion of project initiatives
Demonstrated self-reliance by meeting and exceeding workflow needs

July 1999 to December 2000
Andrex Davie, FL
Pharmaceutical Machine Operator     

Selected proper cutting tools, calculating parameters to manufacture components and parts
Detected work-piece defects and machine malfunctions, maintaining apparatus to prevent future issues
Established and adjusted feed rates and cutting parameters to keep operations in line with production demands.
Documented daily production data and submitted accurate time logs to keep management up-to-date
Obtained appropriate tooling and fixtures to revise setups on calibrated test strands
Set up and operated production equipment according to daily work orders and safety procedures
Operated hand trucks
Recognized defective material and reported issues to management to facilitate prompt resolution
Calibrated and adjusted equipment using tools such as calipers, micrometers, height gauges, protractors and ring gauges
Completed product assembly according to standardized procedures

February 1998 to July 1999
Aveva Miramar, FL
Pharmaceutical Packaging Technician    

Weighed and measured products and materials to check compliance with specifications and regulations
Inspect product components for accuracy by carefully reviewing nicotine patches, packaging, labeling and expiration date
Recorded order processing updates by entering information correctly
Accurately documented package information by completing associated paperwork, attaching labels/barcodes
Operated hand trucks and pallet jacks to move materials
Assembled cartons and crates
Maintained optimal production schedules by safely and quickly operating packaging machinery of various types and at various stages of production lines
Cleared machine jams to reset malfunctions for production line
Removed finished products from machines and separated rejected items based on standard grading qualifications
Accomplished production goals by working with speed and accuracy

Education and Training

December 1984 Jesus Maria Ocampo  Colombia 
High School Diploma  
