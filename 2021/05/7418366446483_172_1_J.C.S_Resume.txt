JASMINE SCOTT  L.B.S.W #63874 
1312 Navasota Ave.
Nederland, Texas 77627
(409) 363-7590 (call or text)
Alt. (409)466-9483 (leave message)
scmch40@aol.com


Career Objectives
Seeking to work in an environment that excels in what they do and believes in helping others. A company which will allow me to grow personally and professionally, work independently and as a collaborative team.

Work History

Baptist Hospital of Southeast Texas BEAUMONT, Texas 
Patient Navigator / L.B.S.W

Mar 15, 2017 - Aug 06, 2020
Assist and aid  social workers to ensure safe discharges for patients. Collaborate with doctors, nurses, and other professionals to evaluate patients' medical or physical condition and to assess client needs. Documentation and discharge planning. Educate and Refer patients to community resources.  Advise patients and family members of Medicare Rights to Appeal a Discharge. Call and Set up home health, snf, nursing home, facility placement, hospice, dialysis,  along with other medical orders. 

GTS Goodwill Temp Services, Houston, Texas 
Data Entry Specialist

Jun 16, 2015 - Apr 04, 2016
E-file and Service Courts Orders and Documents. Enters, corrects, and retrieves information or documents; forwards cases and updates child support case information on TXCSES. Verifies locate and employment information using all available sources. Responds to routine telephone inquiries concerning child support cases. Documentation on every case worked.

Compassionate and Caring Family Services, BEAUMONT, Texas 
Direct Delivery Service Provider (sub-contractor)

Jan 16, 2015 – June 15, 2015
Department of Family and Protective Services Region 5 Contractor as Adult Protective Services case aide (DDSP). interviewing clients during home visits, documenting information gathered, benefit application assistance, ongoing services for Adult Protective Service clients, Case notes, weekly report, needs assessment, Communication and documentation, Social Work training determine benefits client may qualify for, linking clients to resources in community, faxing documents and weekly report to Adult Protective Service worker, transporting of clients, nursing home and housing placement.


Brookshire Brothers, SILSBEE, Texas 
Cook/Cashier

Jan 15, 2008 - Jun 15, 2015
Duties include preparing and cooking various foods, maintaining the deli area along with handling sales transactions with the customer, and providing friendly, courteous customer service.

Attorney General Office, NEDERLAND, Texas 
Intern (Temp)

May 27, 2014 - Aug 13, 2014
Called other states for court documents and information concerning ncp. Enters, retrieves and updates child support case information on TXCSES. Assist child support officers in filing pleadings. Routine calls concerning child support court cases and csrp meetings. Called cp (custodial parent) for information about ncp (non-custodial parent) in order to locate the ncp. Called known employers and other resources of the ncp to help locate.

Texas Home Health, BEAUMONT, Texas 
Healthcare Provider

Jun 02, 2010 - Nov 25, 2010
Maintain records of patient care, condition, progress, or problems to report and discuss 
observations with supervisor or case manager. Provide patients with help moving in and out of beds, baths, wheelchairs, or automobiles and with dressing and grooming. Bathe patients. Care for patients by changing bed linens, washing and ironing laundry, cleaning, or assisting with their personal care.


Dairy Queen, SILSBEE, Texas 
Assistant Manager/Cashier

Jun 10, 2006 - Feb 28, 2007
Communicate effectively with customers regarding orders, comments, and complaints. Prepare daily food items, and cook simple foods and beverages, using proper safety precautions and sanitary measures. Receive payment by cash, check, or credit cards. Issue receipts, refunds, credits, or change due to customers.



Education
Lamar University, Beaumont, Texas 
Bachelor’s Degree of Arts and Sciences
Aug 2011 - Dec 2015
Major: Social Work
Social Work License #63874 (Received Mar. 2017)

Volunteer Experience
2004- Church Volunteer serving food to those in need 
2012 Former volunteer at Pine Harbor Nursing Home


Computer Skills
Typing Speed 35 Words Per Minute 

•	Database Software (Oracle, Access, etc)
•	Data Entry Terminal (PDT, Mainframe Terminal, etc)
•	EMail Software (Outlook, Thunderbird, etc)
•	Graphics or Drawing Software (Photoshop, etc)
•	Internet Browser (Internet Explorer, Firefox, etc)
•	Networking or Lan Software (Cisco, etc)
•	Peripheral Devices (Scanners, Printers, etc)
•	Personal Computers
•	Presentation Software (PowerPoint, Flash, etc)
•	Spreadsheet Software (Calc, Excel, etc)
•	Word Processing Software (Word, WordPerfect, etc)

Language Skills
English - Excellent ( Read,Write,Speak ) 


Driver's License
Class C - Standard Driver's License