
Aja Fly

842 Fernwood Drive
Baldwyn, MS 38824
Phone: 662-706-0833
E-Mail: aja_fly@yahoo.com

Summary

I am a very dedicated person. I always try my best at whatever task that is given to me. I have earned the ACT WorkKeys National Career Readiness Certificate at the Silver level. Energetic individual with a background stocking shelves for various companies. Courteous and cordial and an expert in building client rapport.  Team-oriented Retail Sales Associate who works well in a collaborative environment, yet thrives on personal sales achievements.

Skills

Customer relations
Quality assurance
Cleaning standards
Product displays
Safety methods
Customer rapport
Merchandise stocking
Inventory control procedures
Excellent time management
Approachable
Strong communication skills
Positive outlook

Excellent multi-tasker
Flexible
Detail-oriented
Basic clerical knowledge
High-end fashion knowledge
Dedicated team player
Cheerful and energetic
Reliable and dependable
Organized
Typing speed [number] WPM
Resolution-oriented
Mathematical aptitude

Experience

April 2012 to May 2015
Hancock Fabrics Baldwyn, MS
Double and Roll    
I was responsible for inspecting yards of material for holes, dirt or tears in the fabric. I was also responsible for rolling the specific amount of yards of material onto a fabric board. We were required to get out 2500 yards daily.

Planned and executed [project].

June 2015 to May 2016
APMM Guntown , MS
Stamping    
While working at APMM, in the Stamping department, I was responsible for inspecting and stacking the desired amount of parts to be sent to another department. After working Stamping for several months, I was then placed on a tugger and forklift to distribute the inspected parts to certain departments.

Planned and executed [project].

July 2016 to June 2018
Marathon Cheese Corporation Booneville, MS
Production Worker    
While working at Marathon Cheese, I worked as a production worker. We were responsible for inspecting and packing the desired amount of cheese that the customer ordered. On every Friday or Saturday, depending on the last work day, I was part of the Sanitation team.

Recognized and reported defective material and equipment to [Job title].
Reviewed incoming materials and compared to documentation.
Drove team performance through effective training, coaching and motivating of line employees.
Helped achieve company goals by supporting production workers.
Inspected finished products for quality and adherence to customer specifications.
Maintained clean and organized work areas.

June 2018 to November 2018
Walmart Supercenter Tupelo, MS
Stocker    
I was a part of the CAP-1 team. We stocked shelves and put out the items that were boxed for display.

Properly disposed of debris and box packaging.
Greeted customers and retrieved products that they requested.
Checked shelves, ensuring that they were always fully stocked.
Monitored the work space to ensure that it was neat, orderly and safe.
Blocked and faced all products on shelves and displays to meet company policies.
Kept current on market and product trends to effectively answer customer questions.
Verified that all merchandising standards were maintained on a daily basis.
Shared product knowledge with customers while making personal recommendations.
Built and maintained effective relationships with peers and upper management.
Unloaded trucks, stocked shelves and carried merchandise out on the floor for customers.
Helped customers select products that best fit their personal needs.
Communicated information to customers about product quality, value and style.
Maintained friendly and professional customer interactions.
Answered customers' questions and addressed problems and complaints in person and via phone.
Marked clearance products with updated price tags.
Resolved customer problems by investigating issues, answering questions and building rapport.
Built customer confidence by actively listening to their concerns and giving appropriate feedback.

Education and Training

2001 Baldwyn High School Baldwyn, MS
High School Diploma  

Certifications

ACT WorkKeys NCRC Silver