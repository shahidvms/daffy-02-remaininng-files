Ryan J Hildebrand
Personal Summary
Have foreign and national experience in many different job categories, such as: Retail, Delivery Driver, Sports and Fitness Nutritionist, Fitness Coach, Automotive Salesman, Automotive Private Seller, Supervisor in a digital creator environment.
(510) 690-3126
ryanhildeb@gmail.com
EXPERIENCE
Digital Nomad Support, Bali Supervisor
JUL 2020 - OCT 2020
Supervisor of digital nomads who were in the digital creator/designer fields - would supervise and assist these independent employees when they came and signed up at our support center usually for assistance with their work/projects. Short-lived job position since I was not satisfied with the pay, and I had plans to move to the USA as well.
Motoworld Bali, Denpasar Automotive Salesman
JUL 2017 - MAR 2018
Automotive Salesman at Motoworld Bali, my job duties at this company were to persuade customers into purchasing the motorcycles and other vehicles that were for sale at their showroom that consisted of almost entirely expensive, luxury motorcycles.
Amazon & Doordash, East Bay Area Delivery Driver
OCT 2019 - Present
Delivery Driver for both Amazon and Doordash, done as side jobs for extra income.
Planet Surf, Surabaya Retail Associate
MAR 2017 - JUL 2017
Retail Associate where I worked as a floor staff employee; managing organization/tidiness of the store, along with assisting customers with any inquiries they had while shopping in our store.
EDUCATION
De Anza College, Cupertino International Studies
APR 2018 - Present
Primarily enrolled in this college for extra studies, not with a main purpose of obtaining a degree.
Universitas Ciputra Surabaya, Surabaya Business Marketing
MAR 2018 - APR 2019
Certificate in Business Marketing, similar level to that of a Bachelor s Degree.

PROJECTS
iBolicNutrition, Denpasar Personal Business
AUG 2020 - Present
My recent creation; a website selling bodybuilding/sport supplements based in Indonesia.
SKILLS

Good Time Management
Problem Solving Skills
Project Management
Highly Organized
Time Management
LANGUAGES
English
Indonesian
Private Seller, Denpasar Automotive Private Seller
JAN 2018 - OCT 2020
Automotive reselling group I had created along with several partners (friend & an acquaintance), where we would purchase automobiles and automobile parts and resell them for higher prices. I started this private group while I was still living in Indonesia, but could still manage it fairly easily when I had already moved to the United States.

-----END OF RESUME-----

Resume Title: Supply Chain/Logistics - Quality Control

Name: Ryan Hildebrand
Email: ryanhildeb@gmail.com
Phone: true
Location: Los Angeles-CA-United States-90053

Work History

Job Title: Automotive Salesman 10/01/2019 - Present
Company Name: Motoworld Bali; Amazon & Doordash

Job Title: Supervisor 07/01/2020 - 10/31/2020
Company Name: Digital Nomad Support

Job Title: Automotive Salesman 07/01/2017 - 03/31/2018
Company Name: Motoworld Bali

Job Title: Retail Associate 03/01/2017 - 07/31/2017
Company Name: Planet Surf

Education

School: De Anza College, Cupertino, Major:
Degree: High School, Graduation Date:

School: Universitas Ciputra Surabaya, Surabaya, Major:
Degree: None, Graduation Date:

School: true, Major:
Degree: Bachelor's Degree, Graduation Date:

Additional Skills And Qualifications

Most Recent Job Title: true
Most Recent Pay: USD per
Managed Others: true
Languages Spoken: English
Work Authorization: Not Specified
Security Clearance: No
Felony Conviction: No

Desired Pay: USD per
Desired Job Types: Full Time
Travel Preference:

Currently Employed: No
Jobs Last Three Years: 0
Last Job Tenure Months: 0
Highest Degree: 4 Year Degree
Certifications:
Employment Type:
Military Experience:
Last Updated: 5/4/2021 12:00:00 AM

Desired Shift Preferences:

Interests:
Retail

Downloaded Resume from CareerBuilder
R2Y84H64G596K66DFFR


