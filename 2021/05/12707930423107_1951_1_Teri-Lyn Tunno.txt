TERI LYN TUNNO
104 NEWGATE DRIVE MONACA PA 15061
PHONE: 724-544-8583 E-MAIL: tlt2no@gmail.com

HIGHLIGHT OF QUALIFICATIONS
Self-motivated, enthusiastic, resourceful, committed to getting the job done
Ability to prioritize, delegate and motivate, effective organizational skills
Strong decision-maker, goal oriented, pay attention to details and accuracy
Equally effective working on self-managed projects and as a member of a team
TECHNOLOGICAL EXPERIENCE
Microsoft Excel, Word, Windows, Quickbooks, ADP Payroll, Report Writer,
Jenzabar Software, Calculator 4.6 Speed, CSIU, Great Plains, SAP Business One, ERP1, Karmak, Excede

SUMMARY OF QUALIFICATIONS
Assisted in reducing Receivables greater than 90 days by $500,000 in 4 months.
Researched past due accounts and contacted customers for payment.
Ran batch processing for billings.
Produce monthly financial reports for board meetings amounting from $1 to 57 Mil
Create journal entries as needed to reclassify and update accounts
Review purchases for Capitalization purposes & Use Tax reporting through ETides
Handled all the monthly accounts receivable billing for approximately 26 grant accounts,
35 school billings, and 2 lease billings
Maintained reports on all Program & Job accounts using Excel and QuickBooks
Analyzed over 900 Profit & Loss Statement and Balance Sheet accounts monthly
Reconciled bank statements and fringe benefit invoices
Prepared purchase requisitions and A/P memos for various purchases
Searched and investigate purchases to maximize dollar value
Handled over 2600 college students accounts
Prepared auditor reports for single federal audits as well as the local and state audits
Supervised Accounts Payable, Accounts Receivable, and Payroll staff
Planned cost analysis to estimate changes in personnel, funding, and cash flow
Created and manage budgets for grant programs
Composed training manuals and exhibits for different office positions
Served on Implementation Team to install new computer system at College
Created and update forms in Excel for more efficient processing of information
Participated in team analysis of utilities
Wrote computer programs to produce reports
Trained co-workers in newer software and new hires in office
Designed job manuals for accounts payable and payroll positions

PROFESSIONAL EXPERIENCE
BCRCAliquippa, PA
September 2018 to Present - Support Specialist
Hill International Trucks LLCAliquippa, PA
(Purchased Steel Street Services Inc.)
January 2021 to Present Receptionist (Temporary)

Steel Street Services IncAliquippa, PA
January 2019 to December 2020 Staff Accountant

Pocket Nurse Enterprises, IncMonaca, PA
June 2017 to September 2018 Accounting Assistant

Lincoln Learning Solutions, Inc.Rochester, PA
(Formerly National Network of Digital Schools Management, Inc.)
December 2014 to May 2017 General Ledger Accountant

Cottrill Arbutina & Associates, Beaver, PA
October 2010 to November 2014
Contracted Services Department Staff Accountant
Contracted to work at National Network of Digital Schools

ITC, Inc., Ellwood City, PA
February 2008 to October 2009
Accounting Manager

The Prevention NetworkBaden, PA
(Formerly part of CCBC as the Prevention Project)
August 2005 to February 2008
Fiscal Manager

Community College of Beaver County,Monaca, PA
January 1982 to August 2005
Grant Accountant, Grant Analyst, Payroll Analyst, Accounts Payable, Accounts Receivable

EDUCATION
Geneva College,Beaver Falls, PA
Bachelor of Science Degree
Human Resources, 1993

Community College of Beaver CountyMonaca, PA
Associate in Applied Science Degree
Computer Science, 1984; Accounting, 1981

SERVICE HIGHLIGHTS
Editor and Chief of employee newspaper
Served on CCBC s 30 year anniversary committee
Participated in student mentoring program
Volunteered on Beaver County s Bicentennial Committee
Volunteered at Muddy Rose Pottery assisting teaching with Special Needs
Hold several offices at my Church
Member of my class reunion committee

-----END OF RESUME-----

Resume Title: HR - Generalists

Name: Teri-Lyn Tunno
Email: tlt2no@gmail.com
Phone: true
Location: Monaca-PA-United States-15061

Work History

Job Title: Support Specialist 09/01/2018 - Present
Company Name: BCRC

Job Title: Receptionist 01/01/2021 - Present
Company Name: Hill International Trucks LLC

Job Title: Staff Accountant 01/01/1900 - 04/13/2021
Company Name: Contracted Services Department

Job Title: Staff Accountant 01/01/2019 - 12/31/2020
Company Name: Steel Street Services Inc

Job Title: Accounting Assistant 06/01/2017 - 09/30/2018
Company Name: Pocket Nurse Enterprises, Inc

Job Title: General Ledger Accountant 12/01/2014 - 05/31/2017
Company Name: Lincoln Learning Solutions, Inc.

Job Title: true 10/01/2010 - 11/30/2014
Company Name: Cottrill Arbutina & Associates

Job Title: Accounting Manager 02/01/2008 - 10/31/2009
Company Name: ITC, Inc.

Job Title: Fiscal Manager 08/01/2005 - 02/01/2008
Company Name: The Prevention Network

Job Title: true 01/01/1982 - 08/01/2005
Company Name: Community College of Beaver County

Education

School: Geneva College, Major:
Degree: Bachelor's Degree, Graduation Date:

School: Community College of Beaver County Monaca, PA, Major:
Degree: None, Graduation Date:

Additional Skills And Qualifications

Most Recent Job Title: true
Most Recent Pay: USD per
Managed Others: true
Languages Spoken: English
Work Authorization: Not Specified
Security Clearance: No
Felony Conviction: No

Desired Pay: USD per
Desired Job Types: Full Time
Travel Preference:

Currently Employed: No
Jobs Last Three Years: 0
Last Job Tenure Months: 0
Highest Degree: None
Certifications:
Employment Type:
Military Experience:
Last Updated: 4/13/2021 12:00:00 AM

Desired Shift Preferences:

Interests:
Information Technology
Internet-E-Comerce

Downloaded Resume from CareerBuilder
R2N2BN797K4SFS7JBBF


