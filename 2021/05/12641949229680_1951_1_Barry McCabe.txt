Education and Certifications

Microsoft Certified Systems Administrator (MCSA 2000) - December 2003

Computer Security Institute (CSI) Annual Conference: 17.5 CPE Credits -
November 1999
Washington, DC

Associate of Science Degree (A.S.), Computer Programming Technology -
May 1998
Purdue University
Indianapolis, Indiana

Certified Netware Administrator (CNA4) - August 1997

Experience

Senior Network Engineer - Sasiadek's Information Technologies, Inc.
September 2002 - Current

Duties: Management of an IT department specializing in network design,
installation, and maintenance. Management duties include managing
systems engineers, developing client proposals, client management,
scheduling, and serving as a point of contact between systems
administrators and upper management. Technical duties include
providing network security, server, topology, and workstation support
to the Sasiadek's client base, including large retail organizations and
multi-server accounting environments.

Network Consultant - JCA, Inc.
September 2001 - July 2002

Duties: Provide computer and network maintenance, administration,
installation and troubleshooting for non-profit organizations on an
as-needed basis. Other duties include management and training of new
technicians; network security; relocating major office installations;
network platform migrations; assembling proposals for client
installations; CAT5 network cabling; virus removal and clean-up;
maintenance and support of laptops and handheld devices.

LAN Administrator - LDI, Ltd.
May 1996 - July 2001

Duties: Installation, administration, troubleshooting, and maintenance
of IT at corporate headquarters. Other duties include design of
network topology and installation at national corporate headquarters;
technical liaison to other companies and sources, help desk and user
support, purchasing of computer equipment and supplies, small graphic
arts projects (logos, flyers, etc.) using CorelDraw graphics software,
software training and the creation of user training materials

Technical Support - IUPUI Division of Continuing Studies
February 1993 - May 1996

Duties: Administration of Netware 3.12 LAN; maintain DOS and Windows
workstations; install and maintain application software; purchase and
implementation of new hardware and software; help desk and user
support; technical liaison to outside sources; small graphic arts
projects; student registration and customer support

Skills

Microsoft 2003, 2000 and NT 4.0 server; Netware 5.x, 4.x, 3.x, and
Netware Connect modem pooling server; Microsoft Exchange 2003, 2000 and
5.5 server; Windows XP, 2000, NT, and 98/95 workstations and laptops;
Microsoft Office Suite (2003, XP, 2000, and 97); T1, DSL, and ISDN
routers and firewall appliances including Watchguard Firebox and
Microsoft ISA Server; Microsoft IIS 4, 5 and 6; Citrix Metaframe 1.8
and XP server; GroupWise 5.5 email server; Lotus Domino/Notes V5
groupware server; VPN/IPSec connections; Ethernet switches, hubs, and
bridges; ArcServeIT, Backup Exec and Windows NT Backup software;
antivirus solutions from Norton, McAfee, and Trend Micro; CorelDraw
Graphics Suite (V5 through V10); Lacerte Tax software; SBT Accounting
software; FastTax tax management software; Microsoft Retail Management
Systems software; network printers; Castelle Faxpress fax server;
voicemail to email integration.

-----END OF RESUME-----

Resume Title: Network Administrator

Name: Barry McCabe
Email: a_talk@outlook.com
Phone: true
Location: Tucson-AZ-United States-85716

Work History

Job Title: Senior Network Engineer 09/01/2002 - Present
Company Name: Sasiadek's Information Technologies, Inc

Job Title: Network Consultant 09/01/2001 - 07/01/2002
Company Name: JCA, Inc

Job Title: LAN Administrator 05/01/1996 - 07/01/2001
Company Name: LDI, Ltd

Job Title: Technical Support 02/01/1993 - 05/01/1996
Company Name: IUPUI Division of Continuing Studies

Education

School: Computer Security Institute, Major:
Degree: None, Graduation Date: 12/2003

School: true, Major: Computer Programming Technology
Degree: Associate Degree, Graduation Date: 11/1999

School: Purdue University, Major:
Degree: None, Graduation Date: 5/1998

Additional Skills And Qualifications

Most Recent Job Title: Senior Network Engineer
Most Recent Pay: USD per
Managed Others: Yes
Languages Spoken: English
Work Authorization: Citizen
Security Clearance: No
Felony Conviction: No

Desired Pay: 40000.00USD peryear
Desired Job Types: ["Full Time","Part Time"]
Travel Preference: Up to 25%

Currently Employed: Yes
Jobs Last Three Years: 2
Last Job Tenure Months: 24
Highest Degree: None
Certifications: MCSA Windows 2000,CNA 4 Netware
Employment Type: Employee or Contractor
Military Experience:
Last Updated: 3/26/2021 9:25:21 PM

Desired Shift Preferences: ["First shift (day)","Second shift (afternoon)"]

Interests:
Information Technology
Internet-E-Comerce

Downloaded Resume from CareerBuilder
RC83Q721Y3LDJSP4V8


