Andrea J. Anger

                                                                                                         6 Seaford Place

           Palm Coast, FL 32164

Cell Phone:  702-622-7819     Email:  Andreaanger1@gmail.com

 

Objective Employment with a company offering challenges and opportunities for professional growth.

 

Experience

2019  Dole       Jacksonville, Palm Coast, Daytona

Retail Customer Service Merchandiser

●         Assisted 225 store departments on execution of plan o grams

●         Received in the computer  and counted all inventory

●         Assisted in new hire paperwork and training of new hire employees

●         Maintained cleaning logs and accountability of all restrooms within the department

●         Organized and led all department new product exicution

2014  Citywide Banks               Denver, CO

Treasury Management Services Customer Service Specialist

●         Assisted in initiating direct correspondence  with clients that were large business firms and  small business firms

●         Initiated  and validated  Automatic Clearing House  increases and wire transfers for clients internationally and domestically 

●         Contacted clients to validate, set up, and authorize initial set up procedures for account access through security blocks

●         Complied with Federal banking standard operation procedures

 

2013                       Department of Health and Human Services 

Substance Abuse and Mental Health Services Administration                                                    Rockville, MD

Office of Communications- Intern

●         Processed incoming material which included media, blog, press release, and bulletin control. Researched files and refers them pertaining to their subject matter implementing new material within the department.

●         Prepared a wide variety of internal reports and documents. Controlled incoming and outgoing correspondence to ensure timely processing.

●         Drafted and edited brief letters and memos after receiving general instructions on the subject matter.

●         Prepares collaborative replies to correspondence or reports. Maintains inventory of files.

●         Media and Communications Public Relations Specialist/New Media/Social Media

●         Maintained supervisor’s calendar and reminded them of meetings, also briefed them on the subject matter before the meeting.

 

2012                         Affordable Health Center and Integrated Rehab                                            Palm Coast, FL

Marketing Director

●         Promoted and marketed doctors office and rehab fitness center/$500per. month allowance

●         Contacted and presented at doctors’ offices and lawyers offices  for referrals

●         Booked and promoted at health fairs and government offices

 

2008-2010 Bally Total Fitness Tampa, FL

Front Desk Manager/Member Service Representative

●         Audited all membership contracts and Personal Training contracts

●         Inventoried counts weekly, receive, transfer, and known loss inventory

●         Completed all new hire paperwork for new employees and implemented initial company training components

●         Met retail quotas, membership quotas, EFT conversion quotas

●         Carried large amounts of cash to the Amscot to get a money order daily in order to complete compliance paperwork

 

2007-2008 St. Mary’s Fire Department  St. Mary’s, GA

Fire Fighter

●         Saved lives and prevented over 2 million dollars in property damage

●         Oversaw service contracts and maintained over 11 devices and 3 stations

●         Planned and oversaw maintenance on over 600 fire hydrants saving the city tens of thousands of dollars in corrective maintenance and water costs

●         Conducted Fire Prevention Training for over 450 volunteer and full time fire fighters

●         Proficient in operation of all fire control and safety equipment including hydraulic and cascading systems

●         Federal Emergency Management  Association Risk Management Certification and National Fire Protection Association

 

2006-2007 Belk St. Mary's, GA

Fragrance Manager and Fashion Fair Consultant

●         Responsible for staff training, inventory, display, and customer relations as Department Manager

●         Directly responsible for an average of 100 transactions and $1500 daily revenue per shift

●         Faxed, created spreadsheets, answered three phone lines, scheduled appointments

Volunteer Experience

●         2011-2012    Tampa General Hospital – Case Management/Utilization Review

Education

2013                     National Institute of Occupational Health and Safety   Tampa, FL

2011-Present       University of South Florida B.S. Public Health Degree Tampa, FL

2010-2011 Hillsborough Community College AA Degree           Tampa, FL

2006- 2008 Florida Community College                              Jacksonville, FL

2005                National Fire Protection Association                          Atlanta, GA

2004  Mandeville High School                                      Mandeville, LA

Skills

●         Excellent management skills and understanding of Standard Operating Procedures and policies

●         Outstanding multi task abilities with a strong focus on Customer Service

●         Quick learner, Team-player, 100% dependable and reliable; excellent Public Speaker

●         Superb in MS Word, Excel, Power Point, and Windows with Internet troubleshooting capabilities

 

Graduated Student of the Year Award, Presidential Award, and awarded both Academic and Athletic Scholarships