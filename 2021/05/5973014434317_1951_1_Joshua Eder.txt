Joshua Eder

165 Franklin Street. Apt 107 Bloomfield, NJ 07003

(443)-564-0964

Joshuahart210@yahoo.com

EXPERIENCE:
YOUNGSTUFF, L&F AMERICA, New York, NY

July 2018 to Present

Technical Designer
Responsible for the full life cycle of the brand, from development to TOP, in excel tech packs for Missy and Plus knit
tops, knit bottoms, and woven separates for mass market customers, Walmart and Sam s Club
Lead, review, and evaluate styles with the design team and PD teams to ensure a standardized fit and aesthetic on
live models and customer-issued dress forms
Create detailed technical comments, flats, and sketches to ensure correct pattern adjustments to overseas factories
Initiate conversation of cost engineering with design team to ensure a cost-efficient garment
Build 3D garments in Browzwear for PD line sheets and fit approvals for development and production style- some
experience with CLO

THE MCCALL PATTERN COMPANY, New York, NY

July 2016 to September 2018

Freelance Patternmaker
Design and develop patterns for Cosplay by McCall's
Create fit and photo samples for in-house use and for promotion on the front cover of the pattern
Provide cutters must, materials list, written instruction, and illustrations

DARIAN GROUP INC., New York, NY

October 2013 to July 2018

Associate Technical Designer (January 2018 to July 2018)
Manage and oversee production for Madison Leigh, R&K Dresses, and Laundry by Shelli Segal
Supervise Assistant Technical Designers and domestic sample room of cutters, patternmakers, and sample hands
Create Tech Packs for overseas and domestic production in A2000, includes CMs, graded specs, BOM, mini markers,
and technical sketches
Run production fittings and communicate all fit comments to respective factories via for missy, petite, and plus size
dresses
Inspect and approve TOP samples
Assistant Technical Designer (May 2016 to January 2018)
Oversee technical production for Madison Leigh and R&K Dresses
Create tech packs for overseas and domestic production in A2000, includes CMs, graded specs, BOM, mini markers,
and technical sketches
Assist in production fittings and communicate fit comments to respective factories for missy, petite, and plus size
dresses
Inspect and approve TOP samples
Assistant Designer (October 2013 to May 2016)
Support head designers in RTW designs for plus size, missy, and petite markets for R&K Dresses and Isabella Suits
Organize and fulfill international and domestic sample production; create tech packs
Oversee and track production of 150+ samples per month
Organize and assist sales members with sample style-outs and sample production for clients
Photoshop and Illustrator CADs for Tech Packs or Buyer's Selection

SKILLS
Proficient in Photoshop, Illustrator, Excel, A2000, FlexPLM, Browzwear 3D, patternmaking, draping, tailoring, bridal,
couture, costumery, and sewing. Some experience in CLO.

EDUCATION

Syracuse University, Syracuse, NY

College of Visual and Performing Arts BFA Fashion Design

�

-----END OF RESUME-----

Resume Title: Manufacturing/Production - Project Management

Name: Joshua Eder
Email: joshuaeder@ystuff.com
Phone: true
Location: Bloomfield-NJ-United States-07003

Work History

Job Title: Technical Designer 07/01/2018 - Present
Company Name: YOUNGSTUFF, L&F AMERICA

Job Title: Patternmaker 07/01/2016 - 09/30/2018
Company Name: THE MCCALL PATTERN COMPANY

Job Title: Associate Technical Designer 10/01/2013 - 07/31/2018
Company Name: DARIAN GROUP INC.

Job Title: Assistant Technical Designer 05/01/2016 - 01/31/2018
Company Name: true

Job Title: Assistant Designer 10/01/2013 - 05/01/2016
Company Name: true

Education

School: Syracuse University, Major:
Degree: Bachelor's Degree, Graduation Date:

Additional Skills And Qualifications

Most Recent Job Title: true
Most Recent Pay: USD per
Managed Others: true
Languages Spoken: English
Work Authorization: Not Specified
Security Clearance: No
Felony Conviction: No

Desired Pay: USD per
Desired Job Types: Full Time
Travel Preference:

Currently Employed: No
Jobs Last Three Years: 0
Last Job Tenure Months: 0
Highest Degree: 4 Year Degree
Certifications:
Employment Type:
Military Experience:
Last Updated: 2/23/2021 12:00:00 AM

Desired Shift Preferences:

Interests:
Construction Engineering - Architecture - Surveying
Architecture

Downloaded Resume from CareerBuilder
RDD7V26NH3LHSQ9K5PM


