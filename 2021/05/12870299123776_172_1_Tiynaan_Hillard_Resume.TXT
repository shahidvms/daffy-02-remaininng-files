
Tiynaan Hillard

Macon, Georgia 31211-2323
478-812-2799
hillardtiynaan280@gmail.com

Summary

I am seeking interest in acquiring a technical support or Teir 1 type of entry-level position where I am able to display effective hardware and software implements, troubleshooting procedure & Network Security protection.
I have over 10+ years experience in Computer support. Also, I'm in the process of becoming certified as an MS Office specialist & Comptia Certified.

Skills

• Keyboarding (94 WPM)
• Network troubleshooting measures such as TCP/IP configuration & Firewall installment
•Modem/Router installment/configuration
•Desktop Computer Support/Repair Able to replace any disfunctioning computer part
• (Cmd) Command Prompt configuration
•Virtual Private Network installation
• Cell Phone Repairment •Disk Defragmentation
•Remote Desktop Control
•DNS Resolve
•DHCP setup
•DDR/DDR2 & SDRam DRam implementaion maintenance
•Able to Setup mover +50 workstation all connected to an individual Server Room infrastructure 

Experience

October 2018 to December 2018
Wild Wing's Cafe Macon, Georgia
Dishwasher    

Loaded and unloaded dishwashers, washing by hand large pots or items used on continuous basis.
Kept dishware, glasses and utensils ready for all customer needs by quickly scraping, washing and restacking items.

February 2018 to May 2018
Mikatas Japanese Sushi Bar & Grill Macon, Georgia
Dishwasher    

Loaded and unloaded dishwashers, washing by hand large pots or items used on continuous basis.
Kept dishware, glasses and utensils ready for all customer needs by quickly scraping, washing and restacking items.

August 2016 to November 2016
Agave Azul Mexican Restaurant Macon, GA
Dishwasher    

Loaded and unloaded dishwashers, washing by hand large pots or items used on continuous basis.
Kept dishware, glasses and utensils ready for all customer needs by quickly scraping, washing and restacking items.
Wiped and sanitized various kitchen surfaces using specific cleaning supplies.
Sanitized counters and wiped down surfaces following food preparation to prevent cross-contamination from raw meats.
Repaired and maintained dishwashing machine to keep dishes clean and kitchen running smoothly.
Maintained assigned areas to keep organized and clean by mopping floor, [Task] and [Task] to reduce accidents and sanitize.

Education and Training

2 2017 Virginia College in Macon Ga Macon Georgia
Associate of Applied Science Business Administration 

8 2015 Virginia College in Macon Ga Macon Georgia
College/Diploma Certificate Network Technician 

Certifications

•Perfect attendance Award from Virginia College in Macon