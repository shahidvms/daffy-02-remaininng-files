MELISSA P. BROWN
4425 White Road Douglasville GA 30135 678.517.0485 melissap.brown@comcast.net

Information Technology

Possesses more than 15 years of comprehensive experience within the IT sector, with specialized expertise in hardware (end user and network), software platforms for Windows and Mac based devices, PCs, and mobile devices. Owner of an extraordinary record of success in leadership roles and supervising employees, conducting performance evaluations, managing annual IT budgets and forecasts for software and hardware for senior leaders outside of IT. Skilled in developing a high-performance team with customer-focused training and support. Adapt at cultivating and maintaining strategic relationships.

CORE COMPETENCIES

Customer Service/Satisfaction
Maintaining Records
Scheduling
Motivation/Supervision
Training/Coaching/Development
Accountability
Attention to Detail
Written/Oral Communication
Inventory Control
Teamwork
Technical Competence
Complex Problem Solving
Budget Planning
Time Management
Project Management
IS/Network Security
Systems Testing/Evaluation
Incident Management

NOTABLE ACHIEVEMENTS

Awarded the Employee of the Quarter twice by senior leaders for going above and beyond assigned duties.
Implemented a new ITIL Service Desk ticketing system for helpdesk support that enhanced Service Desk responses to customers, and that separated incidents and requests that were being addressed by the IT staff.
Saved IT department $56,000 annually in consolidating one telecommunication service with another provider.
In 2015, received the Most Valuable Employee award by HR for persistence in managing customer support calls.
As an SAP PO requisitioner for IT network, internet, and telecommunication vendors, helped the IT Director save $50k+ annually in telecommunication costs after sourcing a new vendor with improved quality of service, and a significant reduction of 35% in costs as VoIP.

EXPERIENCE

IT MANAGER | Department of Commerce, US Census Bureau | Atlanta, GA 10/2019 12/11/2020

Observed, monitored, and participated in the maintenance of all computer hardware, software, and telecommunications in the Atlanta Regional Census Center (RCC) and Census offices within 7 states of the region. Provide operational and technical support for the region.
Supported and maintained operations of Cisco 8851 voice communication technologies within the environment and associated sub-systems including, SIP integration, moves, and changes, fault isolation and resolution, and end-user support.
Interacted with 55 internal and external stakeholders as the Subject Matter Expert to provide IT guidance and solutions.
Reviewed requirements of projects to determine objectives of the program, concepts, and processes required.
Organized work processes and solutions to problems adjusting, as needed, for a project/program.
Certified staff hours/overtime, attendance, and various personnel actions.
Worked with HR Manager to facilitate new employee onboarding/termination, trainings, and payroll.
Charged with overseeing office automation functions; serving as the first POC for hardware/software, and telecommunication issues; and with troubleshooting, evaluating, analyzing, and coordinating operations to support office functions more efficiently.
Supported over 450 users of Microsoft products, commercial software, and proprietary US Census programs, creating and deleting users as appropriate, providing access rights to applications, files, and system devices. Provide network support for all relevant company locations, including any remote interconnected sites.
Managed BMC Remedy IT Ticketing Management System to report, track, and resolve issues, and ensure tickets are resolved in less than 72 hours.
Authored end user instruction documents based on basic customer needs.
Diagnosed onsite LAN/WAN hardware for infrastructure cabling and hardware that includes routers, switches, servers, mobile devices, laptops, iPads, personal computers, VoIP telecommunications systems, and printers.
Answerable for the paper/automated tracking of property management such as verifying that necessary forms are completed accurately; property management systems are updated; regular audits are performed; and for securing and storing devices.
Formulated and convey expert recommendations to senior management.
Oversaw the mobile device inventory encompassing iPhones, tablets, and laptops and more than 2k devices for major Census Field Operations - Group Quarters, Transitory Locations, and Non-Responsive Follow-ups.
Preserved interoffice inventory of desktop, laptops, and Cisco VOIP desk phones/headsets, PC accessories, etc.
Provided phone support, email support and in-person support as the initial response to troubleshoot complex problems. Provides technical support assistance to 17 IT infrastructure team members.
Researched, evaluated, and provided feedback on problematic trends and patterns to support customer support requirements.
Identified and specified information system security requirements and ensuring application of information security/information assurance policies, principles and practices are adhered to.
Responsible for the configuration of new equipment and installation or upgrade of both hardware systems and application software.

CONFIGURATION TECHNICIAN (CONTRACTOR) | eBryIT, Inc. | Kennesaw, GA04/2019 10/2019
Employed advanced prioritization and time management abilities to productivity managed assigned territory.
Maintained a positive, self-motivated, and professional attitude when interacting with colleagues and management.
Facilitated training for coworkers/new hires on processes for troubleshooting inoperable devices.
IT INFRASTRUCTURE SPECIALIST | Carestream Dental LLC | Atlanta, GA01/2015 07/2018

Helped the Global Relationship Manager with processes and methods focused on annual IT forecast projects recommended by US senior and executive leaders, such as applications for 3k local and remote employees.
Deployed multiple enterprise applications such as Office 365, Skype for Business, Snow, and Sophos, using Systems Configuration Manager 2012.
Conceptualized the installation/implementation of a new system rollout conversion from Dell to HP laptops.
Coordinated project action items to track when work assignments were completed by key stakeholders.
Executed a domain migration of 3k+ employees.
Recognized as a group SME for supporting executives and HR leadership with confidential IT personnel matters.
Ensured integrity and protection of systems and applications via technical enforcement of organizational security policies and monitoring of vulnerability scanning devices or security scripts, tools, and services.
IT HELPDESK MANAGER | Carestream Dental LLC | Atlanta, GA06/2009 01/2015

Collaborated with departmental units outside of IT to safeguard that groups were educated/trained on recent IT application rollouts.
Sourced, hired, trained, and managed a 10-member Helpdesk team tasked with ensuring continuous quality customer support was proffered to 3k+ national employees; coached direct reports to foster their personal/professional growth.
Orchestrated projects to upgrade local/remote US users to Windows 7 and Microsoft Office 2012.
Successfully maintained an annual CAPEX budget that averaged $800k+ used to conduct system refreshes of laptops and desktops.
As Purchase Order Manager, oversaw IT telecommunications, data services, cellular, and ISP vendor purchase orders that totaled up to $1M.
Effectively employed ERP/SAP applications for financial processes.
Transitioned the helpdesk ticketing system from Microsoft Tasks in Outlook to Track IT to better manage data for end user assets; generated reports that tracked technician ticketing incident closures; set technician resolution timeframes for end users tickets; and revised response times for critical and high priority customer incidents to significantly improve helpdesk ticketing SLAs.
Synchronized the compilation of MS Enterprise Software Licensing to meet annual IT compliance audits.
Completed Project Management Training to help participate in future call center projects.
OTHER EXPERIENCE:
Systems Administrator, Carestream Dental, LLC, Atlanta, GA05/2004 06/2009
Managed Iron Mail Spam server to configure email blacklist and whitelist spam policies.
Decommissioned Dell servers to VMware environments.
Implemented rollout via SCCM of Windows XP and MS Office 2010 Plus Professional.
Provided support and monitoring of security endpoints/servers, e.g., MS Windows OS patch deployment.
Risk assessment
Performed security incident and event monitoring solutions/antivirus products, network security protocols and methodologies.
Assisted IT Director in annual audits for compliance with National Institute of Standards and Technology and International Organization for Standardization frameworks.
Senior Helpdesk Technician, Carestream Dental, LLC, Atlanta, GA10/2000 05/2004
Liaison for entry level helpdesk staff, infrastructure, and telephony teams to assist with IT projects, such as windows 2000 upgrades, connectivity issues with LAN/WAN, VPN, CRM, email servers and VOIP telephony implementation.
including Handled the onboarding of new MS Outlook 2000/Windows 2000 Advanced Server Active Directory accounts, Blackberry accounts and reimaging of new desktops, laptops, or tablets.
Created SOP s for routine Helpdesk tasks and common issues.
BlackBerry Administrator created new users onto BES server and resolved issues with the following Blackberry models 7280 and Blackberry 7100g models used by our executives, sales reps, and trainers of the company.
Worked closely with Telephony Manager to conduct 20% of end users new phone setup and changes within PBX system through OPS Manger.
Worked closely with Network System Administrator to migrate 15% of end users from Win 98 and NT users to Windows 2K Professional.
Assisted in annual Sarbanes-Oxley IT compliance audit.

EDUCATION & CREDENTIALS

Master of Science in Information Systems Management, Health Information Technology
Keller School of Management, Atlanta, GA
Bachelor of Sciences, Computer Science
DeVry Institute of Technology, Decatur, GA
Project Management Training
Six Sigma Training
Quality Advocate Training

AFFILIATIONS

Susan G. Komen Breast Cancer 5K Run/Walk | Habitat for Humanity | Operation Uplift

TECHNICAL PROFICIENCIES

MS Office Suite
Acronis Imaging 2015
IBM Lotus Notes
Coding HTML, Cobol, C++
Ubuntu
MS Exchange Server
Enterprise App Support
SolarWinds
MS Outlook
Windows OS
Palo Alto Global Protect VPN
MS Visio
VMWare
Cisco VPN AnyConnect
MS Project
MS Windows Server
MS Active Directory
RSA Token
Airwatch Workspace One
SAP for Netware v.7.4
Cisco VPN Mobile Client
Remedy Ticket Management
Sophos Antivirus
ITMS dDaaS Asset Custody Tracking/Order Processing
Splunk
Sophos
Citrix Program Neighborhood
Audio/Videoconferencing Setup

-----END OF RESUME-----

Resume Title: Technology

Name: Melissa Brown
Email: melissap.brown@comcast.net
Phone: true
Location: Douglasville-GA-United States-30135

Work History

Job Title: IT MANAGER 10/01/2019 - 11/12/2020
Company Name: Department of Commerce, US Census Bureau

Job Title: CONFIGURATION TECHNICIAN (CONTRACTOR) 04/01/2019 - 10/01/2019
Company Name: eBryIT, Inc.

Job Title: IT INFRASTRUCTURE SPECIALIST 01/01/2015 - 07/31/2018
Company Name: Carestream Dental LLC

Job Title: IT HELPDESK MANAGER 06/01/2009 - 01/01/2015
Company Name: Carestream Dental LLC

Job Title: Systems Administrator 05/01/2004 - 06/01/2009
Company Name: Carestream Dental, LLC

Job Title: Senior Helpdesk Technician 10/01/2000 - 05/01/2004
Company Name: Carestream Dental, LLC

Education

School: Keller School of Management, Major:
Degree: Master's Degree, Graduation Date:

School: DeVry Institute of Technology, Major:
Degree: Bachelor's Degree, Graduation Date:

School: true, Major:
Degree: None, Graduation Date:

Additional Skills And Qualifications

Most Recent Job Title: true
Most Recent Pay: USD per
Managed Others: true
Languages Spoken: English
Work Authorization: Not Specified
Security Clearance: No
Felony Conviction: No

Desired Pay: USD per
Desired Job Types: Full Time
Travel Preference:

Currently Employed: No
Jobs Last Three Years: 0
Last Job Tenure Months: 0
Highest Degree: None
Certifications:
Employment Type:
Military Experience:
Last Updated: 5/12/2021 12:00:00 AM

Desired Shift Preferences:

Interests:
Information Technology
Internet-E-Comerce

Downloaded Resume from CareerBuilder
RD901J5XJXKPB650038


