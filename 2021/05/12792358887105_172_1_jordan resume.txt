Jordan W. Rocchi

8475 Appalichaian Hwy RT 10

P.O.  Box 175 Jesse WV 24849

Home number 304-682-3257

Cell phone number 304-923-3157

Education:

1.      Graduated from Wyoming East High School Class 2005.

2.      Attended Wyoming County Career and Technical Center studied Diesel Technology.

3.      Took 80 Hr underground mining class March 10, 2008.

4. Took the wild well service class in November 5, 2014.

5. Took HSE Rig Pass orientation 

January 30,2014

6. Took CPR, AED, and First Aid for adults 

7. Took OSHA Standard Training

Fall protection, hydrogen sulfide, Respiratory  Protection, SPCC, Hazcom in January 31 2014.

8. Equipment Operator Certification  . Telehandler October 30,2014. Frontend loader October 30,2014.

Job Experience :

1. worked for Patterson UTI as a floor hand an derrick man for three years 

2.worked for falcon drill as a floor hand 2 years

3. Worked for well service group as a floorhand, operator, derrickhand and a driller for a schramm  rig for 1 year and 2 months. 

4. Worked for savanna drilling as a Lead hand, operator and motorman 12 months 

5. Worked for James River Coal Mines Coal Company underground : Boltman 13 months, Buggie man for 2 months,  Scoopeman for 6 months,  3rd Shifts Move Crew on Super 

6. Worked for Pinpoint Drilling for 12 months as : Lead Floor Hand.

7. Worked for GasField Services for 18 months as a : Saw hand and Dozer Operator.

8.Worked for Antler Welding for 6 months as a Saw hand and Dozer Operator.

9. Worked for Union Drilling for 13 months as a : Derrickman and Crew Leader.

10. Worked for Sam W Jack for 14 months as : Floorhand and Laborer. 

11.worked for baker installations 

In beckley wv Pineville wv and Brenton,wv Welch Wv, Roanoke va.

 

References:

1.      Lewis  W. Lester               304-682-3388

2.      James Osborne                304-664-2387

3.      Jerry green

1 (276) 365-5695

4.      Lydge Burns                      304-732-8774

5.      Amos Duty                        304-732-8045 (304) 923-7089