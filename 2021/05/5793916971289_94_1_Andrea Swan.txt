Andrea Swan
(678) 548-9514 and13swa@gmail.com |

Andrea Swan
Duluth, GA 30096 (678) 548-9514 and13swa@gmail.com

Insurance industry professional
Travel Claims | Property and Casualty | Claims Auditor

Accomplished, versatile and solutions-focused professional with strong expertise in Insurance claims. Leverages strong technical expertise and excellent interpersonal skills with a strong customer service orientation and the ability to solve problems and manage projects. Collaborative team member with extensive experience working to help clients to resolve complex claims. Adept at working with all levels of management and staff to gain cooperation and promote teamwork. Core competencies:

Travel ClaimsISO Reporting Process Improvement Skills
Healthcare Claims International ClaimsProperty and Casualty
Payment Authorization Performance MetricsLoss Paid Reports Financials

Professional Overview

BERKSHIRE HATHAWAY SPECIALTY INSURANCE, ATLANTA GA2018 - Present
Claims Operations Analyst
Successfully provided a wide variety of services for casualty insurance client base. Regularly interfaced with brokers and insureds to implement and administer Property and Casualty insurance claims. Key Contributions:

Processed healthcare and casualty claims from start to finish, verifying and updating information on submitted claims within established turn-around times with high quality and accurate data entry.
Reviewed and analyzed policies for new claim loss reports to determine claim type and appropriate policy coverage related to primary and excess policy coverages.
Acknowledged claims to adjusters, insureds and brokers in a professional, accurate and timely manner according to account specific guidelines.
Administered support to the Travel Claims team by taking calls and creating new losses.

BERKSHIRE HATHAWAY SPECIALTY INSURANCE, ATLANTA GA2017 - 2018
Travel Claims CSR
Regularly interfaced with insurance customers and travel agents to implement and administer travel insurance claims. Worked on special projects and provided system support to clients and other cross functional teams. Key Contributions:
Provided superior customer service and support to external and internal customers of the Travelex and BHTP Client Group.
Entered new loss information into Salesforce, including losses that may be phoned into the company or emailed by an insured, or an agent.
Accurately indexed claim documents to the proper claims file in the companys claim management system.
Administered support for claims adjusters and other system support functions.

BERKLEY SOUTHEAST INSURANCE GROUP, Lawrenceville GA2015 - 2017
Claims Technician
Successfully provided a wide variety of services for a large client base. Regularly interfaced with vendors and clients to implement and administer workers compensation and commercial lines insurance claims. Worked on special projects and provided system support to clients and other cross functional teams. Key Contributions:
Provided superior customer service and support to external and internal customers of the Claims Creation Unit.
Entered new loss information into BSIGs claims management system, including losses that may be phoned into the company or emailed by an insured, a claimant or an agent. Directed those losses to the proper claims handling unit.
Accurately indexed electronic mail to the proper claims file in the companys claim management system.
Administered support for claims adjusters and staff accountants, which includes entering new vendors into system once the W-9 form is received, verification of checks, and other system support functions.

AIG, Alpharetta, GA2008 - 2015
Provider Bill Analyst, 2014-2015
Successfully provided a wide variety of services for a large client base. Regularly interfaced with vendors and clients to implement and administer workers compensation and other insurance claims. Worked on projects and provided data reporting to clients and other cross functional teams. Key Contributions:
Exceeded departmental expectations with an audit score of 99.5% which saved the company millions of dollars.
Reviewed and analyzed workers compensation bills submitted by medical professionals and hospitals.
Ensured that charges were appropriate according to state jurisdictional medical fee
schedule guidelines.
Applied appropriate payment recommendations and routed bills to the necessary department
for additional review and maintenance when required.

World Source Claims Analyst, 2011-2014
Served as the Litigation Support Specialist. Reconciled Loss Paid Reports for more than 10 foreign countries while ensuring compliance with state and federal law. Key Contributions:
Recognized for exceeding expectations with an average quality score of 100%.
Handled the posting of international claim advices, reserve revisions, re-opens and the closing of claims.
Authorized international claim payments in accordance with established procedures on a daily basis.
Reconciled Loss Paid Reports Financials for several foreign countries.

Property & Casualty Research Analyst 2008-2011
Delivered planning, direction, and coordination of all aspects of document quality. Monitored daily operations to ensure customer expectations were met. Managed all information provided by client and updated files timely and accurately. Key Contributions:
Exceeded expectations with a quality score of 99.47%.
Identified and researched multiple lines of businesses for Property & Casualty on a daily basis (construction defect, asbestos, toxic tort, marine, Lexington, and first notice of lost for Property & Casualty).
Handled lawsuit complaints daily. Contacted the insureds, attorneys and P&C boards for information necessary to make a proper identification.
Member of 3-person team selected to set up severity and punitive damage claims.

Academic Credentials & Professional Development

Property & Casualty Pre-Licensing 40-Hour course certificate; AINS Designation in progress
Business Administration, Old Dominion University, Norfolk, VA

Technical Skills

Microsoft Office Suite of Products (Word, Excel, Access, and PowerPoint); Outlook; 50 WPM, including 10-key

-----END OF RESUME-----

Resume Title: Claims Operations Analyst

Name: Andrea Swan
Email: andreaswan920@gmail.com
Phone: (678) 548-9514
Location: Duluth-GA-United States-30097

Work History

Job Title: Claims Operations Analyst 01/01/2018 - Present
Company Name: BERKSHIRE HATHAWAY SPECIALTY INSURANCE

Job Title: Travel Claims CSR 01/01/2017 - 01/01/2018
Company Name: BERKSHIRE HATHAWAY SPECIALTY INSURANCE

Job Title: Claims Technician 01/01/2015 - 01/01/2017
Company Name: BERKLEY SOUTHEAST INSURANCE GROUP

Job Title: Provider Bill Analyst 01/01/2008 - 01/01/2015
Company Name: AIG

Job Title: World Source Claims Analyst 01/01/2011 - 12/31/2014
Company Name: true

Job Title: Property & Casualty Research Analyst 01/01/2008 - 01/01/2011
Company Name: true

Education

School: Old Dominion University, Major:
Degree: Bachelor's Degree, Graduation Date:

Additional Skills And Qualifications

Most Recent Job Title: true
Most Recent Pay: USD per
Managed Others: true
Languages Spoken:
Work Authorization: Not Specified
Security Clearance: No
Felony Conviction: No

Desired Pay: USD per
Desired Job Types: ["Full Time","Part Time","Intern","Seasonal","Temporary","Contractor"]
Travel Preference:

Currently Employed: No
Jobs Last Three Years: 0
Last Job Tenure Months: 0
Highest Degree: 4 Year Degree
Certifications:
Employment Type:
Military Experience:
Last Updated: 5/7/2021 8:54:56 AM

Desired Shift Preferences:

Interests:
General Business
Supply Chain
Business Development
Consultant
Strategy - Planning
Telecommunications
BPO-KPO

Downloaded Resume from CareerBuilder
RD63266RGGW9SFBVZ54


