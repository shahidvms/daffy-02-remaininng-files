BRANNON FEB
brannonfeb@gmail.com
Pipefitter Foreman/ Pipefitter 
Hendrix OK 74741
(580)513-0156

Job Objective
To obtain Pipefitter Foreman position in which I can utilize my prior experience to asset the reputation and growth of the organization.

Professional Qualifications
*NCCER #20467232
*OSHA
*TWIC
*Heavy Industrial Construction Maintenance and Repair
*Solid knowledge of all safety aspects
*Complete understanding of time management, project details, quality assurance, production standards

Project Types
Heavy Industrial Maintenance, New Construction, Turnarounds and Shutdowns.
*Power Plants
*Chemical Plants
*Gas Plants
*Refineries
*Clean Room & Instrumentation


Professional Work Experience

Steamfitter Foreman
Butters-Fetting
Jan 2020-07/2020
New Construction in all piping at Casino/ Hotel
Conduct Quality Assurance/Control inspections on work completed and resolve any deficiencies found.
Supervised building of hot and cold water mechanical rooms and cooling towers. Installation of Chill water supply and return for the deicer unit, large and small bore pipe installation.
Supervise a crew of 5. Maintain safety and time management is all aspects.


Pipefitter Foreman
June 2019- Jan 2020
Great Arrow Builders
Fabrication and installation of small and large bore pipe for several systems in plant
Installation of various valves and supports
Supervise crew of 8 responsible for daily welding and foreman progress reports
Responsible for underground, above ground and flare piping.
Responsible for crew safety, daily productivity and schedule.
Well known for getting along with people and getting the job done safely and on time. Conduct Quality Assurance/Control inspections on work completed and resolve any deficiencies found.
June 2019- Jan 2020

Pipefitter/ Pipe Foreman
Zachry Industrial
June 2018-June 2019
New Construction at Valero
New Diesel fuel refinery
Ardmore, Ok


Pipefitter
Billifinger-Ames, Iowa
May 2018-June 2018
New Construction, Clean room, Stainless Pipe

Pipefitter
QLM-Sherman,Tx
February 2018-April 2018
New Construction/Clean Room and Instrumentation and Stainless pipe at Finisar Corporations

Pipefitter
Systems Group-Durant, Ok
February 2017 to May 2019
New Construction at new facility for CMC metals. Built pipe and rack as specified. Read and interpret blue prints. Stainless steel. Welding Tig root and 7018 out. Attach pipes to walls and structures. Measure mark and cut pipe. Shutdowns/Turnarounds

Pipefitter/Boilermaker
USI - Liberal, KS Ponca City, Ok
March 2014 to 2017
Attached pipes to walls, structures and fixtures, such as radiators or tanks, using brackets, clamps, tools or welding equipment. Measuring, mark and cut pipe. Bolt up, shutdowns turnaround.


Pipefitter/ Boilermaker
BHI- Oolagah, OK Siloam Springs AR Borger, Tx
May 2014 to September 2016
Attach pipes to walls, structures and fixtures, such as radiators or tanks, using brackets, clamps, tools or welding equipment. Measured and mark pipes for cutting, threading and welding also use of specific tools such as saws, cutting torches, pipe threaders, benders, and welders. Shutdowns, new construction, and Turnarounds.


Boilermaker
Amec Foster Wheeler - Longview, WV
Assembled and installed pipe systems, pipe supports, and hydraulic and pneumatic equipment. Installed pipe for steam, hot water, heating, cooling, and industrial. Hydro testing.
Nov 2014 to January 2015

Pipefitter
FHI Plant Services - Amarillo, TX
November 2012 to February 2014
Pipefitter duties/shutdown. Weld, read and interpret blue prints, bolt up. Assemble and install pipe to walls and fixtures. Shutdowns.

Education
Spartan School of Aeronautics - Tulsa, OK
Associates Degree
Aviation Maintenance Technician
NDT testing and QC for Aviation Maintenance
08/2000 to 05/2002

