Mallory Bass-Senegal
Houston, TX
832.641.6968
mallorysenegal@yahoo.com

Professional Experience
Southside Infusion
Client Relations Manager
2019- 2020

Managed the clincal staffing plan to ensure the appropriate staffing
levels for current and forecasted business
Generated bid proposal for clinical projects
Implented a standardized training program and trained staff on
clinical best practices
Planned and executed all Infusion nursing events
Managed all clinical events planning and budget
Managed vendor relationship and neogated vendor contracts
Ensured effective use of clincal resources, clinical equipment and
clinical supplies.
Served as liaison between physicians and staff regarding work
assignments, clinical priorities, and performance issues
Managed the day to day operations
Managed inventory and quarlerty inventory reporting

Altus Infusion, Houston, TX
Senior Healthcare Staffing Specialist
2018- 2019

Managed the full cycle recruitment process for the North American
region
Developed a robust staffing schedule strategies for clinical
department
Coordinated, planned, and implemented clinical staffing needs and
travel logistics for clinical staff coverage.
Managed weekly, monthly and annual recruitment plan to ensure business
objectives were met
Completed new hire pre-employment screens such as background, drug
screens, references etc. within in company policy
Implemented a candidate touchpouch program to promoted candidate
engagement
Developed and managed the employee referral program
Developed relationships with travel nurses to help place them in open
needs across the United States.
Oversaw the new hire training and orientation schedule
Strategize with the executive leadership team to accomplish staffing
growth goals

Memorial Hermann TMC, Houston, TX
Senior Staffing Coordinator
2009 - 2017

Review and approve schedule change request
Obtains census, acuity and activity levels for 33 units on a shift by
shift basis
Determines staffing requirements for each nursing unit based on 24-
hour coverage
Communicate campus staffing plan with Directors and Managers to ensure
safe staffing for the campus
Create shift huddle report in excel daily, which gives a 24 hour snap
shot of needs for the hospital staffing
Review and edit contract employee timecards
Review contract labor invoices and submit check request
Function as a resource to managers and directors for efficient use of
staffing/scheduling system
Orients and trains on boarding travel nurses regarding system and
hospital policies and procedures
Compiled statistics and data for reporting purposes and maintained
census reports.
Ensures appropriate patient flow by admitting/transferring/discharging
patient in computer system
Scheduled patient testing delivered specimens to lab
Ensured company policies of budgetary specifications are followed,
including time/supply management, and accuracy

Education
University of Houston -B.S. Consumer Science,Houston TX 2015

Skills
Software: Microsoft Office; Ultipros; Taleo; PeopleSoft; ICIMS; Oracle
Familiarity with various Vendor Management Systems
Proven leadership ability with a competitive drive
Excellent interpersonal and organizational skills
Strong presentation and communication skills
Effective time management and problem solving skills

-----END OF RESUME-----

Resume Title: Healthcare - Allied Health - Administration

Name: Mallory Senegal
Email: mallorysenegal@yahoo.com
Phone: true
Location: Houston-TX-United States-77271

Work History

Job Title: Client Relations Manager 01/01/2019 - 12/31/2020
Company Name: Southside Infusion

Job Title: Senior Healthcare Staffing Specialist 01/01/2018 - 01/01/2019
Company Name: Altus Infusion

Job Title: Senior Staffing Coordinator 01/01/2009 - 12/31/2017
Company Name: Memorial Hermann TMC

Education

School: University of Houston, Major:
Degree: Bachelor's Degree, Graduation Date:

Additional Skills And Qualifications

Most Recent Job Title: true
Most Recent Pay: USD per
Managed Others: true
Languages Spoken: English
Work Authorization: Not Specified
Security Clearance: No
Felony Conviction: No

Desired Pay: USD per
Desired Job Types: Full Time
Travel Preference:

Currently Employed: No
Jobs Last Three Years: 0
Last Job Tenure Months: 0
Highest Degree: 4 Year Degree
Certifications:
Employment Type:
Military Experience:
Last Updated: 4/14/2021 12:00:00 AM

Desired Shift Preferences:

Interests:

Downloaded Resume from CareerBuilder
R2R5Z875VQP1F5B4SJT


